<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Script ver senha -->
    <script type="text/javascript">


        function toggle(button) {
        var password = document.getElementById("password");

            if (password.type == "password") {
                button.innerHTML = "";
                password.type = "text";
            }
                else {
                    button.innerHTML = "";
                    password.type = "password";
                }
        }

        function togglerAtual(button){
            var senhaAtual = document.getElementById("senhaAtual");

            if (senhaAtual.type == "password") {
                button.innerHTML = "";
                senhaAtual.type = "text";
            }
                else {
                    button.innerHTML = "";
                    senhaAtual.type = "password";
                }
        }

        function togglerNova(button){
            var novaSenha = document.getElementById("novaSenha");

            if (novaSenha.type == "password") {
                button.innerHTML = "";
                novaSenha.type = "text";
            }
                else {
                    button.innerHTML = "";
                    novaSenha.type = "password";
                }
        }

        function togglerConfirma(button){
            var confirm = document.getElementById("confirm");

            if (confirm.type == "password") {
                button.innerHTML = "";
                confirm.type = "text";
            }
                else {
                    button.innerHTML = "";
                    confirm.type = "password";
                }
        }

    </script>

     <!-- Script Mascara -->

     <script type="text/javascript">
            function fMasc(objeto,mascara) {
                obj=objeto
                masc=mascara
                setTimeout("fMascEx()",1)
            }
            function fMascEx() {
                obj.value=masc(obj.value)
            }
           
            function mCPF(cpf){
                cpf=cpf.replace(/\D/g,"")
                cpf=cpf.replace(/(\d{3})(\d)/,"$1.$2")
                cpf=cpf.replace(/(\d{3})(\d)/,"$1.$2")
                cpf=cpf.replace(/(\d{3})(\d{1,2})$/,"$1-$2")
                return cpf
            }

           function mCEP(cep){
                cep=cep.replace(/\D/g,"")
                cep=cep.replace(/^(\d{2})(\d)/,"$1.$2")
                cep=cep.replace(/\.(\d{3})(\d)/,".$1-$2")
                return cep
            }

            function mRG(rg){
                rg=rg.replace(/\D/g, "")
                rg=rg.replace(/(\d{1})(\d)/, "$1.$2")
                rg=rg.replace(/(\d{3})(\d)/, "$1.$2")
                rg=rg.replace(/(\d{3})(\d{1,2})$/, "$1.$2")
                return rg

            }

            function mTel(contato) {
                contato=contato.replace(/\D/g,"")
                contato=contato.replace(/^(\d)/,"($1")
                contato=contato.replace(/(.{3})(\d)/,"$1)$2")
                if(contato.length == 9) {
                    contato=contato.replace(/(.{1})$/,"-$1")
                } else if (contato.length == 10) {
                    contato=contato.replace(/(.{2})$/,"-$1")
                } else if (contato.length == 11) {
                    contato=contato.replace(/(.{3})$/,"-$1")
                } else if (contato.length == 12) {
                    contato=contato.replace(/(.{4})$/,"-$1")
                } else if (contato.length > 12) {
                    contato=contato.replace(/(.{4})$/,"-$1")
                }
                return contato;
            }
    </script>

    <script src="https://code.jquery.com/jquery-3.2.1.min.js"
            integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
            crossorigin="anonymous"></script>

    <!-- Adicionando Javascript -->
    <script type="text/javascript" >

        $(document).ready(function() {

            function limpa_formulário_cep() {
                // Limpa valores do formulário de cep.
                $("#cep").val(null);
                $("#logradouro").val(null);
                $("#bairro").val(null);
                $("#cidade").val(null);
                $("#uf").val(null);
            }

            //Quando o campo cep perde o foco.
            $("#cep").blur(function() {

                //Nova variável "cep" somente com dígitos.
                var cep = $(this).val().replace(/\D/g, '');

                //Verifica se campo cep possui valor informado.
                if (cep != "") {

                    //Expressão regular para validar o CEP.
                    var validacep = /^[0-9]{8}$/;

                    //Valida o formato do CEP.
                    if(validacep.test(cep)) {

                        //Preenche os campos com "" enquanto consulta webservice.
                        $("#logradouro").val("Encontrando cep...");
                        $("#bairro").val("");
                        $("#cidade").val("");
                        $("#uf").val("");

                        //Consulta o webservice viacep.com.br/
                        $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                            if (!("erro" in dados)) {
                                //Atualiza os campos com os valores da consulta.
                                $("#logradouro").val(dados.logradouro);
                                $("#bairro").val(dados.bairro);
                                $("#cidade").val(dados.localidade);
                                $("#uf").val(dados.uf);
                            } //end if.
                            else {
                                //CEP pesquisado não foi encontrado.
                                limpa_formulário_cep();
                                alert("CEP não encontrado.");
                            }
                        });
                    } //end if.
                    else {
                        //cep é inválido.
                        limpa_formulário_cep();
                        alert("Formato de CEP inválido.");
                    }
                } //end if.
                else {
                    //cep sem valor, limpa formulário.
                    limpa_formulário_cep();
                }
            });
        });

    </script>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- ICONES -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
</head>

<style type="text/css">
    html,
body {
  height: 100%;
}
#page-content {
  flex: 1 0 auto;
}
</style>

<body class="d-flex flex-column">

<div id="page-content">

    @if(!Auth::guest())
    <nav class="navbar navbar-expand-md navbar-light navbar-laravel fixed-top shadow-sm">  

            <div class="nav-header" 
            style=" background-color:#3490dc; 
            margin-right: 0;
            margin-left: -16px; 
            padding: 10px 10px;
            margin-top: -10px; 
            margin-bottom: -8px;">

                <span class="navbar-brand" style="color: #fff;">
                    Sin-SAMU
                </span>

            </div>

            
            <div class="navbar-collapse">

                     
                <button class="navbar-toggler float-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}" style="margin-top: -40px;">
                <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        
                        <li>
                            <a href="{{route('home')}}" class="nav-link">Dashboard</a>
                        </li>
                        <div class="btn-group">
                            <a  class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                            style="cursor:pointer">
                                Cadastros
                            </a>
                            <div class="dropdown-menu">
                                <li>
                                    <a href="{{route('func')}}" class="nav-link"> Funcionários </a>
                                </li>
                                <li>
                                    <a href="{{route('estabelecimento')}}" class="nav-link"> Estabelecimentos </a>
                                </li>
                       
                                <li>
                                    <a href="{{route('frota')}}" class="nav-link"> Frotas </a>
                                </li>
                                
                            </div>
                        
                        </div>

                        <li>
                            <a href="{{route('home')}}" class="nav-link">Relatórios</a>
                        </li>
                        @endif
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                    
                        @else
                            <li class="nav-item dropdown">

                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{\App\Http\Controllers\FuncController::authName()}} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                                    <a class="dropdown-item" href="{{route('minhaconta')}}">
                                        <i class="fas fa-user"></i> Minha Conta</a>
                                    
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                       <i class="fas fa-sign-out-alt"></i> {{ __('Sair') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                    </ul>
                </div>
            </div>
        </nav> 
        
        @endguest
         
        <main class="py-5">
            
            @yield('content') 
          
        </main>

    
</div>

    <footer id="sticky-footer" style="background-color: #247CB5;">

         <div class="container">
            <img src="https://sinprocesso.infortechms.com.br/img/logo-datasus.1544065129.png" height="40px;" width="160px;">

            <img src="https://sinprocesso.infortechms.com.br/img/logo-governo.1544065129.png" height="47px;" width="243px;" style="float: right;">
        </div>

    </footer>
</body>
</html>

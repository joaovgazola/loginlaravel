@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row m-t-40 justify-content-center">
        <div class="col-md-6 col-md-offset-6">
            <div class="card" style="background-color: #E1EAEB;">
                

                <div class="card-body" style="align-items: center;">

                    <h3 class="text-center">Resetar Senha</h3>

                    <form method="POST" action="{{ route('password.update') }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group col-12">
                            <strong>
                               <label for="email" class="control-label">
                               {{ __('E-Mail') }}
                               </label> 
                            </strong>

                            <div class="input-group">

                                <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"
                                style="background-color: rgb(232, 240, 254);">
                                <i class="fas fa-envelope"></i>
                                </span>
                                </div>

                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus readonly 
                                style="background-color: rgb(232, 240, 254);">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group col-12">
                            <strong>
                               <label for="email" class="control-label">
                               {{ __('Senha') }} <strong style="color: red;">*</strong>
                               </label> 
                            </strong>

                            <div class="input-group">

                                <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"
                                style="background-color: rgb(232, 240, 254);">
                                <i class="fas fa-lock"></i>
                                </span>
                                </div>

                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password"  
                                style="background-color: rgb(232, 240, 254);">

                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1"
                                    style="background-color: rgb(232, 240, 254);">
                                    <i class="fa fa-eye"
                                     onclick="toggle(this);"></i>
                                    </span>
                                </div>

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group col-12">
                            <strong>
                               <label for="email" class="control-label">
                               {{ __('Confimar Senha') }} <strong style="color: red;">*</strong>
                               </label> 
                            </strong>

                            <div class="input-group">

                                <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"
                                style="background-color: rgb(232, 240, 254);">
                                <i class="fas fa-lock"></i>
                                </span>
                                </div>

                                <input id="confirm" type="password" class="form-control" name="confirm" required autocomplete="new-password" 
                                style="background-color: rgb(232, 240, 254);">

                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1"
                                    style="background-color: rgb(232, 240, 254);">
                                    <i class="fa fa-eye"
                                     onclick="togglerConfirma(this);"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="text-center">
                            
                                <button type="submit" class="btn btn-primary btn-block">
                                    {{ __('Resetar Senha') }}
                                </button>
                           
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

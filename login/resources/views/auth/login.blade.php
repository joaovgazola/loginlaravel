@extends('layouts.app')

@section('content')

    <div class="row col m-t-40 justify-content-center">
        <div class="col-md-4 col-md-offset-4">
            <div class="card" style="background-color: #E1EAEB;">
                <div class="card-body">
                    <h2 class="text-center m-b-20"><b>Sin-SAMU</b></h2>
                    <br>

                    @if (Session::has('alter_email'))
                            <div class="alert alert-info" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                 <span aria-hidden="true">&times;</span>
                                </button>
                                <i class="fas fa-check-circle"></i>
                                {{Session::pull('alter_email')}}

                            </div>
                    @endif

                    @if (Session::has('alter_perfil'))
                            <div class="alert alert-info" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                 <span aria-hidden="true">&times;</span>
                                </button>
                                <i class="fas fa-check-circle"></i>
                                {{Session::pull('alter_perfil')}}

                            </div>
                    @endif
                    @if (Session::has('alter_cpf'))
                            <div class="alert alert-info" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                 <span aria-hidden="true">&times;</span>
                                </button>
                                <i class="fas fa-check-circle"></i>
                                {{Session::pull('alter_cpf')}}

                            </div>
                    @endif
                    <form method="POST" action="{{ route('login') }}" 
                     class="col-md-12 p-t-10">
                        @csrf

                        <div class="form-group">

                            <strong>
                                <label class="control-label" for="email">CPF</label>
                            </strong>
                            <div class="input-group">
                              <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"
                                style="background-color: rgb(232, 240, 254);">
                                <i class="fa fa-user"></i>
                                </span>
                              </div>
                                <input id="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}"
                                    style="background-color: rgb(232, 240, 254);"
                                     required 
                                     autofocus
                                     placeholder="000.000.000-00" 
                                     maxlength="14"
                                     onkeydown="javascript: fMasc( this, mCPF );">
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                            </div>          
                        </div>

                        <div class="form-group">

                            <strong>
                            <label for="password" class="control-label">Senha</label>
                            </strong>

                            <div class="input-group">
                              <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"
                                style="background-color: rgb(232, 240, 254);">
                                <i class="fas fa-lock"></i>
                                </span>
                              </div>
                                 <input type="password" class="form-control" name="password" id="password" style="background-color: rgb(232, 240, 254);">

                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1"
                                    style="background-color: rgb(232, 240, 254);">
                                    <i class="fa fa-eye"
                                     onclick="toggle(this);"></i>
                                    </span>
                                </div>

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror          
                            </div>

                        <div class="form-group">
                           
                                <div class="checkbox">
                                    <br>
                                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Lembre-me') }}
                                </div>
                            
                        </div>

                        <div class="form-group row">
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <button type="submit" class="btn btn-primary btn-block btn-sm">
                                        Entrar
                                    </button>
                                </div>
                                <br>

                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    @if (Route::has('password.request'))
                                        <a class="btn btn-link" style="margin-top: -4px;" href="{{ route('password.request') }}">
                                            Esqueceu sua Senha?
                                        </a>
                                    @endif
                                </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection
@extends('layouts.tarm')

@section('content')
<script>
  $(document).ready(function(){
    var paciente = $("#pacientes").html();

    $(".deletarPaciente").click(function(){
        $(this).parent().parent().remove();
      });

    $(".adicionarPaciente").click(function(){
      $('#pacientes').append(paciente);
      $(".deletarPaciente").click(function(){
        $(this).parent().parent().remove();
      });
    });
  });

</script>
   


<div class="container">
  <div class="row justify-content-center">
    <div class="col-12">
      <form method="post" action="{{route('chamadoSalvar')}}">
      {{ csrf_field() }}
        <br>
        <div class="row col-lg-12">
          <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
              <!-- ocorrencia -->
              <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">
                  <h4  class="text-dark"> Ocorrências</h4>
              </a>
            </li>
            <!-- paciente -->
            <li class="nav-item">
              <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">
                <h4 class="text-dark"> Paciente </h4>
              </a>
            </li>
            <!-- ultimas chamadas -->
          </ul>
        </div> 

        <div class="card">  
          <div class="card-body ml-1">
            <div class="tab-content" id="myTab">
              <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                
                  <!-- ocorrencia -->
                  <div class="row col-12"  style="padding: 0; margin: 0;"> 
                    <div class="col-lg-2">
                        <strong>
                            <label for="protocolo">
                                N° da Ocorrência
                              </label>
                        </strong>
                        <input type="text" name="protocolo" id="protocolo" class="form-control"  
                             placeholder="00000000-0"> 
                        <br>
                    </div>
                    <!-- telefone -->
                    <div class="col-lg-2">
                      <strong>
                          <label for="contato">
                              Telefone
                          </label>
                      </strong>
                      <input type="text" name="contato" id="contato" 
                        class="form-control"
                        placeholder="(00)00000-0000" 
                        maxlength="14"
                        onkeydown="javascript: fMasc( this, mTel );" autofocus><br>
                    </div>
                    <!-- nome do solicitante -->
                    <div class="col-lg-3">
                        <strong>
                          <label for="nomeSolicitante">Nome do solicitante
                          </label>
                        </strong>
                        <input type="text" name="nomeSolicitante" id="nomeSolicitante" class="form-control" pattern="[a-z A-Z À-ú]+$" title="Digite somente Letras"> <br>
                    </div>
                    <!-- tipo do chamado -->
                    <div class="col-lg-3">
                      <strong>
                        <label for="tipoChamado">Tipo do Chamado
                        </label>
                      </strong> 
                      <select name="tipoChamado" id="tipoChamado"
                      class="form-control">
                          <option selected disabled value="">
                            Selecione
                          </option>
                              <option value="Informação"> Informação </option>
                              <option value="Pedido de ajuda"> Pedido de Ajuda </option>
                              <option value="Transporte"> Pedido de Transporte </option>
                              <option value="Trote"> Trote </option>
                              <option value="Engano"> Engano </option>
                              <option value="Outros"> Outros </option>

                      </select><br>
                    </div>
                      <!-- Apelido -->
                      <div class="col-lg-2">
                          <strong>
                              <label for="apelido"> Apelido
                              </label>
                          </strong>
                          <input type="text" name="apelido" id="apelido" class="form-control"  title="Digite somente Letras"> <br>
                      </div>


                    <!-- fato -->
                    <div class="col-lg-12 col-sm-12 col-md-12">
                      <strong>
                        <label for="fato">Fato</label>
                      </strong>

                      <textarea class="form-control" rows="5" cols="50"
                        name="fato" id="fato" placeholder="Descrição da Ocorrência"></textarea>
                    </div>
                  </div>
                  <br>
                    <!-- dados do endereço da solicitação -->

                  <div class="row col-12" style="padding: 0; margin: 0;">
                    <div class="col-lg-6 float-left d-flex align-content-between flex-wrap" style="padding: 0; margin: 0;">
                      <!-- cep -->
                      <div class="col-lg-4">
                        <strong>
                          <label for="cep">CEP</label>
                          
                        </strong>
                          <input type="text" name="cep" id="cep"
                          class="form-control mapa"
                          placeholder="00000-000"
                          maxlength="10"
                          onkeydown="javascript: fMasc( this, mCEP );">
                          <a href="http://www.buscacep.correios.com.br/sistemas/buscacep/" target="_blank"> Não sei meu CEP </a>
                      </div>
                      <!-- logradouro -->
                      <div class="col-lg-8">
                            <strong>
                              <label for="logradouro">Logradouro</label>
                            </strong>

                          <input type="text" name="logradouro" id="logradouro" class="form-control mapa">
                          
                      </div>
                      <!-- numero -->
                      <div class="col-lg-4">
                        <strong>
                          <label for="numero">Número
                        </label>
                        </strong>
                        <input type="number" name="numero" id="numero" class="form-control mapa">
                      </div>
                      <!-- bairro -->
                      <div class="col-lg-8">
                          <strong>
                              <label for="bairro">Bairro</label>
                          </strong>
                          <input type="text" name="bairro" id="bairro" class="form-control mapa">
                      </div>
                        <!-- estado -->
                      <div class="col-lg-4">
                          <strong>
                              <label for="uf">Estado</label>
                          </strong>
                          <input type="text" name="uf" id="uf" class="form-control mapa"><br>
                      </div>
                              <!-- cidade -->
                      <div class="col-lg-8">
                          <strong>
                              <label for="cidade">Cidade</label>
                          </strong>
                          <input type="text" name="cidade" id="cidade" class="form-control mapa">
                      </div>
                      
                      <!-- referencia -->
                      <div class="col-lg-12">
                        <strong>
                            <label for="referencia">Referência</label>
                        </strong>
                        <input type="text" name="referencia" id="referencia" class="form-control"><br>
                      </div>

                    </div>


                    <!-- mapa -->
                    <div class="col-lg-6 float-right col-lg-6 float-left d-flex align-content-between flex-wrap" style="padding: 0; margin: 0;">

                      <div id="busca_loc" class="col-lg-12">
                        <strong>
                            <label for="loc">Pesquisar</label>
                        </strong>
                        <input id="loc" type="text" placeholder="Insira o endereço" class="form-control">
                        <input  id="latitude" placeholder="Latitude"/>
                        <input  id="longitude" placeholder="Longitude"/>
                      </div>

                      <div id="map" style="padding: 0; margin: 0;"></div>

                      <div id="infowindow-content">
                        <img src="" width="20" height="20" id="place-icon">
                          <span id="place-name"  class="title"></span><br>
                          <span id="place-address"></span>
                      </div> 
                    </div>

                  </div>

                  <br>
                  <div class="row col-lg-12" style="padding: 0; margin: 0;">
                    <div class="col-lg-6" style="padding: 0; margin: 0;">
                      <!-- prioridade -->
                      <div class="col-lg-12">
                        <strong>
                          <label for="prioridade">Prioridade
                          </label>
                        </strong>
                        <select name="prioridade" id="prioridade"
                          class="form-control">
                          <option selected disabled value="">
                            Selecione
                            </option>
                                <option value="Emergência"> Emergência </option>
                                <option value="Urgência Moderada"> Urgência Moderada</option>
                                <option value="Urgência Baixa"> Urgência Baixa</option>
                                <option value="Urgência Mínima"> Urgência Mínima</option>
                                <option value="Urgência Mínima"> AMUV</option>
                          </select><br>
                      </div>
                      <!-- fim prioridade -->
                      <!-- medico -->
                      <div class="col-lg-12">
                          <strong>
                            <label for="medico">Médico
                              </label>
                          </strong>
                          <div class="input-group mb-3">
                              <select name="medico" id="medico" class="form-control">
                                  <option selected disabled value="">
                                    Selecione
                                    </option>
                                    @foreach($Medicos as $Medico)
                                      <option value="{{$Medico->id}}">{{$Medico->nome}}</option>
                                    @endforeach
                                    </select>
                              <div class="input-group-prepend">
                                  <label class="input-group-text" for="inputGroupSelect01">Auto</label>
                              </div>
                          </div>
                          <br>
                      </div>
                      <!-- fim medico -->
                    </div>
                      <!-- observações -->
                    <div class="col-lg-6 float-right" style="padding: 0; margin: 0;">
                      <div class="col-lg-12">
                        <strong>
                          <label for="obervacoes">
                            Observações
                          </label>
                        </strong>
                        <textarea class="form-control" rows="5" name="observacoes" id="obs-med" placeholder="Observações para o Médico"></textarea><br>
                      </div>
                    </div>
                    <!-- Fim observações -->
                  </div>
              </div>

              <!-- tela paciente -->
              <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                  <div clas="row col-lg-12" id="pacientes">

                      <div class="row col-lg-12" id="paciente" style="margin:10px">

                            <div class="col-lg-3">
                              <strong> <label>Nome do Paciente</label></strong>
                              <input type="text" name="paciente" class="form-control">
                            </div>

                            <div class="col-lg-1">
                              <strong> <label> Idade </label> </strong>
                              <input type="number" name="idade" class="form-control">
                            </div>

                            <div class="col-lg-2">
                              <strong>
                                <label>
                                  Classificação Etária
                                </label>
                              </strong>
                              <select name="faixaEtaria" id="faixaEtaria" class="form-control">
                                <option selected disabled value="">
                                  Selecione
                                </option>
                                <option value="Neonatal">Neonatal</option>
                                <option value="Criança">Criança</option>
                                <option value="Idoso">Idoso</option>
                                <option value="Adulto">Adulto</option>
                              </select>
                            </div>

                            <div class="col-lg-2">
                              <strong>
                                <label>
                                  Sexo
                                </label>
                              </strong>
                              <select name="sexo" id="sexo" class="form-control" required>
                                <option selected disabled value="">
                                  Selecione
                                </option>
                                <option value="Masculino">Masculino</option>
                                <option value="Feminino">Feminino</option>
                                <option value="Outros">Outros</option>
                              </select>
                            </div>

                            <div class="col-lg-3">
                              <strong>
                                <label for="incidentePaciente">
                                  Incidente
                                </label>
                              </strong>
                              <textarea class="form-control" rows="1" cols="50" name="incidentePaciente" id="incidentePaciente"></textarea>
                            </div>

                          <div class="col-lg-1">
                              <a href="#" style="margin-top:30px; magin-right:10px;" class="btn btn-danger btn-md deletarPaciente" title="Deletar">
                                  <i class="fas fa-trash-alt"></i>
                              </a>
                          </div>
                      </div>
                  </div>
                  <br>
                  <div class="row">
                    <div class="col-lg-12">
                      <a class="btn btn-primary float-right adicionarPaciente" href="#" style="margin-top:10px">Adicionar Paciente  <i class="fa fa-plus"></i></a>
                    </div>
                  </div>
              </div>
               <!-- fim tela paciente -->

            </div>
          </div>
        </div>

        <nav class="navbar navbar-expand-md navbar-light bg-light navbar-laravel shadow-sm col-lg-12 border ">
          <b>
          <a href="#" class="nav-link text-dark"> Ações </a>
          </b>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <!-- BARRA DE AÇÕES -->
          <div class="collapse navbar-collapse" id="navbarText">
              <ul class="navbar-nav col-lg-12">
                <li class="nav-item col-lg-3">
                    <button type="reset" class="btn bg-transparent text-muted" style="display: inline; margin: 0 65px;" ><b> Limpar </b></button>
                </li>

                <li class="nav-item col-lg-3">
                  <strong>
                      <a class="nav-link text-muted" href="#" data-toggle="modal" data-target=".bd-example-modal-lg"> Ultimas Chamadas </a>
                  </strong>
                </li>

                <li class="nav-item col-lg-3">
                  <strong>
                      <a class="nav-link text-muted" href="#">Transferir Solicitações</a>
                  </strong>
                </li>

                <li class="nav-item col-lg-3">
                  <strong>
                      <button type="finalizar" class="btn bg-transparent text-muted" ><b> Finalizar Chamada </b></button>
                  </strong>
                </li>

              </ul>            
          </div>

          <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title">Ultimas Chamadas</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                      <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body table-responsive-lg">
                  <table class="table table-hover">
                    <thead style="text-align: center;">
                      <tr>
                        <th scope="col"> Vincular </th>
                        <th scope="col">Telefone</th>
                        <th scope="col">Bairro</th>
                        <th scope="col">Tipo</th>
                        <th scope="col">Fato</th>
                        
                      </tr>
                    </thead>
                    <tbody style="text-align: center;">
                        
                      <tr>
                        <td> <input type="radio" name="vincula"> </td>
                        <td>example</td>
                        <td>example</td>
                        <td>example</td>
                        <td>example</td>
                          
                      </tr>
                        
                    </tbody>
                  </table>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">
                    Cancelar
                  </button>
                  <input type="submit" name="vincula" class="btn btn-primary" value="Vincular">
                </div>
              </div>
            </div>
          </div>
            </div>
          </div>
        </nav>
      </form>           
    </div>
  </div>  
</div>
@endsection
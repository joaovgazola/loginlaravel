@extends('layouts.app')

@section('content')
<script>
    $(document).ready(function(){

        //botão editar tipo: esconde add e mostra edit de tipo
        $("#btnEditTipo").click(function(){
            $("#addTipo").collapse('hide');
            $("#editTipo").collapse('toggle');
        });

        //botão adicionar tipo: esconde edit e mostra add de tipo
        $("#btnAddTipo").click(function(){
            $("#addTipo").collapse('toggle');
            $("#editTipo").collapse('hide');
        });

        //Importa o tipo selecionado para o editar
        $( "#tipo" ).change(function() {
            var editdescricao =  $( "#tipo option:selected" ).text();
            $("#editdescricao").val(editdescricao);
            var editid =  $( "#tipo option:selected" ).val();
            $("#editid").val(editid);
        });
        
        //submit ajax Adcicionar Tipo
        $("#btnAdicionar").click(function(){
            var descricao = $("#descricao").val();
            var token = $("#token").val();
            $.ajax({
                type: "POST",
                data: "descricao=" + descricao + "&_token=" + token,
                url: "{{route('tiposalvar')}}",
                success:function(){
                    $("#addTipo").collapse('hide');
                    $("#descricao").val("");
                }
            })

            //atualiza select
            $.ajax({
                type: "get",
                url:"{{route('buscatipo')}}",
                async: false,
                dataType:"json",
                success:function(data){
                    var i;
                    var select = "<select name='tipo' class='form-control' required> <option selected disabled value=''> Selecione </option>";
                    for(i=0; i < data.length; i++){
                        select += "<option value='"+data[i].id+"'>"+ data[i].descricao +"</option>";
                    }
                    select += "</select>";
                    $('#tipo').html(select);
                }
            })
               
        });

    });
</script>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-12">
            <br>
            <div class="card">
                <div class="card-header">

                        <h2> Cadastro de Frota </h2>
                 </div>

                    <div class="card-body">
                        <div class="alert alert-info" role="alert">
                            Os campos marcados com <strong> * </strong> são de preenchimento obrigatório.
                        </div>
                            <div class="container col-12">
                                 <h3> Dados da Frota</h3>

                                <li style="border-top: 2px #efefef solid; margin-top: 0px; margin-bottom: 0px; display: block;"> </li>
                                <br>


                                @if (Session::has('erro'))
                                    <div class="alert alert-danger" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                         <span aria-hidden="true">&times;</span>
                                        </button>
                                        <i class="fas fa-times-circle"></i>
                                        {{ Session::pull('erro') }}
                                    </div>
                                @endif
                                @if (Session::has('sucesso-frota'))
                                    <div class="alert alert-success" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                         <span aria-hidden="true">&times;</span>
                                        </button>
                                        <i class="fas fa-check-circle"></i>
                                        {{ Session::pull('sucesso-frota') }}
                                    </div>
                                @endif


                                <!--Campo do adicionar tipo-->

                                    <div class="collapse multi-collapse" id="addTipo">
                                        <div class="card card-body">

                                            <input type="hidden" value="{{ csrf_token() }}" id="token">   
                                                <h3> Adicionar tipo do Veículo</h3>
                                                <li style="border-top: 2px #efefef solid; margin-top: 0px; margin-bottom: 0px; display: block;"> </li>
                                                <br>
                                                <strong>
                                                    <label> Veículo
                                                        <strong style="color: red;">*</strong>
                                                    </label>
                                                </strong>

                                                <input type="text" name="descricao" id="descricao" class="form-control" required pattern="^[a-z A-Z À-ú]+$">

                                                <br>
                                                <div>
                                                    <div class="float-right">
                                                        <input type="submit" name="adicionar" id="btnAdicionar" class="btn btn-primary" value="Adicionar">
                                                    </div>

                                                    <div class="float-right" style="padding-right: 5px; ">
                                                        <a class="btn btn-danger" style="color:#fff" href="" onclick="window.location.href=">Cancelar</a>
                                                    </div>
                                                </div>
                
                                        </div>
                                        <br>
                                    </div>
                                <!--Fim do Campo adicionar veículo-->

                                <!--Campo do aditar veículo-->
                                    <div class="collapse multi-collapse" id="editTipo">
                                        <div class="card card-body">
                                            <form action="{{route('tipoupdate')}}" method="post">
                                                {{ csrf_field() }}

                                                <h3> Editar tipo do Veículo</h3>
                                                <li style="border-top: 2px #efefef solid; margin-top: 0px; margin-bottom: 0px; display: block;"> </li>
                                                <br>
                                                <strong>
                                                    <label> Veículo
                                                        <strong style="color: red;">*</strong>
                                                    </label>
                                                </strong>

                                                <input type="text" name="editdescricao" id="editdescricao" class="form-control" required>
                                                <input type="hidden" name="editid" id="editid" pattern="^[0-9]+$">
                                                <br>
                                                <div>
                                                    <div class="float-right">
                                                        <input type="submit" name="editar" class="btn btn-primary" value="Editar">
                                                    </div>
                                                    <div class="float-right" style="padding-right: 5px; ">
                                                        <a class="btn btn-danger" style="color:#fff" href="" onclick="window.location.href=">Cancelar</a>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <br>
                                <!--Fim do Campo adicionar veículo-->

                                <form method="post" action="{{route('frotasalvar')}}"
                                id="multiple_select_form">
                                    {{ csrf_field() }}
                                    <div class="row">
                                            <div class="col-lg-2 col-sm-2 col-md-2">
                                                <strong>
                                                    <label for="numero">Número
                                                        <strong style="color: red;">*</strong>
                                                    </label>
                                                </strong>
                                                <input type="number" name="numero" id="numero" class="form-control" autofocus required
                                                min="1"><br>
                                            </div>

                                            <div class="col-lg-2 col-sm-2 col-md-2">
                                                <strong>
                                                    <label for="placa">Placa
                                                        <strong style="color: red;">*</strong>
                                                    </label>
                                                </strong>
                                                <input type="text" name="placa" id="placa" class="form-control" required style="text-transform: uppercase;"
                                                maxlength="8"><br>
                                            </div>

                                            <div class="col-lg-3 col-sm-3 col-md-3">
                                                <strong>
                                                    <label for="quilometragem">Quilometragem
                                                        <strong style="color: red;">*</strong>
                                                    </label>
                                                </strong>
                                                <input type="number" name="quilometragem" id="quilometragem" class="form-control" required
                                                min="1"><br>
                                            </div>

                                            <div id="tipoSelect" class="col-lg-5 col-sm-5 col-md-5">
                                              <strong>
                                                <label for="tipo">Tipo
                                                    <strong style="color: red;">*</strong>
                                                </label>
                                              </strong>  
                                                <div class="input-group">
                                                
                                                <select name="tipo" id="tipo" class="form-control" required>
                                                     <option selected disabled value="">
                                                        Selecione
                                                     </option>
                                                 @foreach($tipos as $tipo)
                                                     <option value="{{$tipo->id}}">{{$tipo->descricao}}</option>
                                                  @endforeach
                                                </select>

                                                <div class="input-group-append">

                                                    <button class="btn btn-mute dropdown-toggle " type="button hidden" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="fas fa-cog"></i>
                                                    </button>
                                                    <div class="dropdown-menu btn-inline">
                                                      <a class="dropdown-item" id="btnAddTipo" href="#addTipo" role="button" aria-expanded="false">
                                                        Adicionar 
                                                        </a>
                                                      <a class="dropdown-item" id="btnEditTipo" href="#editTipo" role="button" aria-expanded="false">
                                                        Editar 
                                                    </a>
                                                      
                                                    </div>
                                                  </div>
                                                  
                                                </div> 
                                            </div>
                                        </div>

                                        <div>
                                            <br>
                                            <div class="float-right">
                                                <input type="submit" name="cadastro" class="btn btn-primary" value="Cadastrar">
                                            </div>
                                        </div>

                                </form>

                                 <div class="float-right" style="padding-right: 5px;">
                                    <button class="btn btn-primary" onclick="window.location.href='/frota'"><a href="/frota" style="color: #fff; text-decoration: none;">Cancelar</a></button>
                                </div>

                                <!---->

                        </div>


                </div>
            </div>
        </div>
    </div>
</div>


@endsection
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 col-sm-12 col-lg-12">
                <br>
                <div class="card">
                    <div class="card-header">
                        <h2>
                            Editar Estabelecimento
                        </h2>
                        <br>
                        <div class="panel with-nav-tabs panel-primary">
                            <div class="panel-heading">
                                <ul class="nav nav-tabs" style="margin-bottom: -13px;">
                                    <li class="active nav-link">

                                        <a href="#" style="text-decoration: none; color: #212529;">
                                        Estabelecimento </a>

                                    </li>
                                    <li class="disabled nav-link">
                                        Especialidade
                                    </li>

                                </ul>
                            </div>

                        </div>


                    </div>

                    <div class="card-body">
                        <div class="alert alert-info" role="alert">
                            Os campos marcados <strong> * </strong> são de preenchimento obrigatório.
                        </div>

                        <div class="container col-12">
                                    <h3>Dados do Estabelecimento</h3>

                                <!--Linha da divisão-->
                            <li style="border-top: 2px #efefef solid; margin-top: 0px; margin-bottom: 0px; display: block;"></li>

                            <br>
                                <!-- Dados do Estabelecimento-->
                            <form method="POST" action="{{route('estabUpdate', ['id'=>$estab->id])}}">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <strong>
                                            <label for="nome"> Nome
                                                <strong style="color: red;">*</strong>
                                            </label>
                                        </strong>
                                        <input type="text" name="nome" value = "{{$estab->nome}}"   class="form-control" autofocus required>
                                    </div>   
                                </div>                           
            
                        

                            <!-- Dados do Local-->
                            <br>
                            
                             <br>
                                <h3>Endereço</h3>

                                <!--Linha da divisão-->
                                <li style="border-top: 2px #efefef solid; margin-top: 0px; margin-bottom: 0px; display: block;"></li>

                                <br>
                                <div class="row">

                                    <div class="col-lg-2 col-md-2 col-sm-2">
                                        <strong>
                                            <label for="cep">CEP
                                                <strong style="color: red;">*</strong>
                                            </label>
                                        </strong>

                                        <input type="text" name="cep" id="cep" value = "{{$local->cep}}"
                                        class="form-control"
                                        placeholder="00000-000" 
                                        maxlength="10"
                                        onkeydown="javascript: fMasc( this, mCEP );" required>


                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <strong>
                                        <label for="logradouro">Logradouro </label>
                                        </strong>
                                        <input type="text" name="logradouro" id="logradouro" value = "{{$local->logradouro}}" class="form-control" readonly>
                                    </div>

                                    <div class="col-lg-2 col-md-2 col-sm-2">
                                        <strong>
                                            <label>Número   
                                                <strong style="color: red;">*</strong>
                                            </label>
                                        </strong>
                                        <input type="number" name="numero" id="numero" value = "{{$local->numero}}"class="form-control " >
                                    </div>

                                    <div class="col-lg-3 col-sm-3 col-md-3">
                                        <strong>
                                            <label for="cidade">Cidade</label>
                                        </strong>
                                        <input type="text" name="cidade" id="cidade" value = "{{$local->cidade}}"class="form-control" readonly>
                                    </div>

                                    <div class="col-lg-1 col-sm-1 col-md-1">
                                        <strong>
                                            <label for="uf">Estado</label>
                                        </strong>
                                        <input type="text" name="uf" id="uf" value = "{{$local->uf}}" class="form-control" readonly>
                                    </div>


                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <br>
                                        <strong><label for="bairro">Bairro</label></strong>
                                        <input type="text" name="bairro" id="bairro" value = "{{$local->bairro}}" class="form-control " readonly>
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <br>
                                        <strong><label>Referência</label></strong>
                                        <input type="text" name="referencia" value = "{{$local->referencia}}" class="form-control ">
                                    </div>
                                </div>

                                <!--Button-->
                                <br>
                                <div>
                                    <div class="float-right">

                                       <input type="submit" name="salvar" class="btn btn-primary" value="Salvar"> 
                                    </div>
                                    
                                    
                            
                                </div>
                      
                        </form>

                        <div class="float-right" style="padding-right: 5px;">
                            <button class="btn btn-primary" onclick="window.location.href='/estabelecimento'"><a href="/estabelecimento" style="color: #fff; text-decoration: none;">Cancelar</a></button> 
                        </div>
                    </div>

                 </div>
            </div>
        </div>
    </div>

@endsection
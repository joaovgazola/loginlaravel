@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-12">
          <br>          
            <div class="card">
                <div class="card-header"> <h2> Funcionários</h2></div>

                  <div class="card-body">
                      @if (Session::has('sucesso-funcionario'))
                          <div class="alert alert-success" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                          </button>
                              <i class="fas fa-check-circle"></i>
                            {{Session::pull('sucesso-funcionario')}}

                          </div>
                      @endif
                      @if (Session::has('falha-funcionario'))
                        <div class="alert alert-danger" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                          </button>
                            <i class="fas fa-times-circle"></i>
                          {{Session::pull('falha-funcionario')}}
                        </div>
                      @endif

                      @if (Session::has('edit-funcionario'))
                          <div class="alert alert-success" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                          </button>
                              <i class="fas fa-check-circle"></i>
                            {{Session::pull('edit-funcionario')}}

                          </div>
                      @endif
                  <div id="accordionFilter">

                    <div class="mb-0">
                        <button class="btn btn-link bg-white " data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" style="text-decoration: none;">
                        Filtros<i class="fas fa-filter btn-sm"></i>
                        </button>
                        
                        
                          
                        <li style="border-top: 2px #efefef solid; display: block;"> </li>
                           
                      </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionFilter">

                    <div class="jumbotron jumbotron-fluid"
                    style="background-color: rgba(0,0,0,.03); box-shadow: 0px  0px 1px #000000;">

                      <div class="container">
                        
                        <form action="{{route('funcbusca')}}" method="post">
                            {{ csrf_field() }}
                            <div class="row col-12" style="margin-top: -35px;">
                                
                                <div class="col-lg-3 col-sm-3 col-md-3">
                                    <strong>
                                      <label for="nome">Nome</label>
                                    </strong>
                                    <input type="text" name="nome" id="nome" class="form-control" autofocus pattern="^[a-z A-Z À-ú]+$" title="Digite somente Letras"><br>
                                 </div>

                                 <div class="col-lg-3 col-sm-3 col-md-3">
                                    <strong>
                                      <label for="cpf">CPF</label>
                                    </strong>
                                    <input type="text" name="cpf" class="form-control"
                                    placeholder="000.000.000-00" 
                                    maxlength="14"
                                    onkeydown="javascript: fMasc( this, mCPF );"><br>
                                 </div>

                                 <div class="col-lg-2 col-sm-2 col-md-2">
                                    <strong>
                                      <label for="matricula">Matrícula</label>
                                    </strong>
                                    <input type="text" name="matricula" id="matricula" class="form-control" autofocus pattern="^[0-9]+$" title="Digite somento Números"><br>
                                 </div>

                                 <div class="col-lg-2 col-sm-2 col-md-2">
                                    <strong>
                                      <label for="perfil">Perfil</label>
                                    </strong>
                                      <select name="perfil" id="perfil" class="form-control">
                                          <option disabled selected>Selecione</option> 
                                        <option value="Tarm">TARM</option>
                                        <option value="Médico Regulador">Médico Regulador</option>
                                        <option value="Rádio Operador">Radio Operador</option>
                                        <option value="Gestor">Gestor</option>
                                        <option value="Enfermeiro">Enfermeiro</option>
                                        <option value="Motorista">Motorista</option>
                                    </select><br>
                                 </div>

                                 <div class="col-lg-2 col-sm-2 col-md-2">
                                    <strong>
                                      <label for="status">Situação</label>
                                    </strong>
                                      <select name="status" id="status" class="form-control">
                                          <option disabled selected>Selecione</option> 
                                        <option value="true">Ativo</option>
                                        <option value="false">Inativo</option>
                                    </select><br>
                                 </div>

                          </div>
                          <br>
                          <div class="container">

                                <button type="submit" name="pesquisar" title="Pesquisar" class="btn btn-primary" value="Pesquisar">
                                <i class="fas fa-search"></i>
                                Pesquisar
                                
                                </button>

                                <button type="reset" name="limpar" title="Limpar pesquisa" class="btn btn-primary" value="Limpar">
                                    <i class="fas fa-undo-alt"></i>
                                 Limpar 
                                  
                                </button>
                            </div>
                          
                          </form>
                          

                        </div>
                      </div>
                    </div>
                    
                </div>                        
                <br>
      <!---------------------------------------------------------------------------------->
                        <div class="container col-12">

	                        	<div class="row">

	                        		<div class="col-md-6 col-sm-6 col-lg-6">
	                        			<h2 class="float-left"> Lista </h2>
	                        		</div>

	                        		<div class="col-md-6 col-sm-6 col-lg-6">

	                        			<button type="button" class="btn btn-primary float-lg-right"
                                onclick="window.location.href='/funcionario/novo'">
                                <i class="fas fa-plus"></i>
                                Novo Funcionário
                                
                                </button>
	                        		 </div>
	                        		

	                        	</div>
	                        	
	                    </div>

              			<li style="border-top: 2px #efefef solid; margin-top: 0px; margin-bottom: 0px; display: block;"> 
              			</li>
       
                        <br>

                        <div class="table-responsive">
                        <table class="table table-hover table-bordered"> 
                            <thead style="background-color: rgba(0,0,0,.03); text-align: center;">
                                <tr>
                                  <th>Nome</th>
                                  <th>CPF</th>
                                  <th>Matrícula</th>
                                  <th>Perfil</th>
                                  <th>Situação</th>
                                  <th colspan="3" width="1%">Ações</th>

                           
                                </tr>
                              </thead>
                           
                        
                              <tbody style="text-align: center;">
                              @foreach($func as $funcionario)
                                <tr>
                        
                                  <td>{{$funcionario->nome}}</td>
                                  <td>{{$funcionario->cpf}}</td>
                                  <td>{{$funcionario->matricula}}</td>
                                  <td>{{$funcionario->perfil}}</td>
                                  @if($funcionario->status == 1)
                                        <td>
                                        Ativo
                                        </td>
                                        <td>
                                            <a href="{{route('funcstatus', ['id'=>$funcionario->id])}}" class="btn btn-danger btn-sm" title="desativar"
                                              onclick="return confirm('Tem certeza que deseja desativar {{$funcionario->nome}}?');">
                                                <i class="fa fa-ban"></i>
                                                Desativar
                                            </a>
                                        </td>
                                    @else
                                        <td>
                                         Inativo
                                        </td>
                                        <td>
                                            <a href="{{route('funcstatus', ['id'=>$funcionario->id])}}" class="btn btn-success btn-sm" title="ativar">
                                                <i class="fa fa-check"></i>
                                                Ativar
                                            </a>
                                        </td>
                                        @endif
                                  
                                  <td>
                                    <a href="/funcionario/{{$funcionario->id}}/edit" class="btn btn-primary btn-sm" title="Editar"> 
                                      <i class="fas fa-edit"></i>
                                    </a>

                                  </td>

                           
                                </tr>
                              @endforeach
                              </tbody>
                        

                        </table>
                    </div>
                    <br>

                </div>
            </div>
        </div>
    </div>
</div>

@section('scripts')
<script src="{{ asset('js/mascara.blade.php')}}"></script>
@endsection

@endsection

@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 col-sm-12 col-lg-12">
                <br>
                <div class="card">
                    <div class="card-header">
	                     <h2> 
	                        Editar Funcionário 
	                    </h2>

                    </div>
        
                    <div class="card-body">
                        @if (Session::has('erro-cadastro'))
                            <div class="alert alert-danger" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                {{Session::pull('erro-cadastro')}}
                            </div>
                        @endif

                        <div class="alert alert-info" role="alert">
                            Os campos marcados com <strong> * </strong> são de preenchimento obrigatório.
                        </div>

                        <div class="alert alert-warning" role="alert">
                            <b>NOTA:</b> Caso o Perfil, CPF ou E-mail do usuário logado seja alterado o usuário será redirecionado para tela de Login.
                             <i class="fas fa-exclamation-triangle"></i>
                        </div>

                        
                    <div class="container col-12">

                        <h3> Dados do Funcionário </h3>

                        <li style="border-top: 2px #efefef solid; margin-top: 0px; margin-bottom: 0px; display: block;"> </li>
                        <br>
                        <!-- FORM CADASTRO DE FUNCIONARIOS-->
                        
                            <form method="post" action="/funcionario/{{$func->id}}/update">
                                {{ csrf_field() }}
                                <div class="row">

                                    <div class="col-lg-4 col-sm-4 col-md-4">
                                        <strong>
                                            <label for="nome">Nome
                                                <strong style="color: red;">*</strong>
                                            </label>
                                        </strong>
                                        <input type="text" name="nome" value="{{$func->nome}}" id="nome" class="form-control" autofocus required
                                        pattern="^[a-z A-Z À-ú]+$" title="Digite somente Letras">
                                    </div>

                                     <div class="col-lg-4 col-sm-4 col-md-4">
                                        <strong>
                                            <label for="cpf">CPF
                                                <strong style="color: red;">*</strong>
                                            </label>
                                        </strong>
                                        <input type="text" name="cpf" value="{{$func->cpf}}" class="form-control"
                                        placeholder="000.000.000-00" 
                                        maxlength="14"
                                        onkeydown="javascript: fMasc( this, mCPF );" required>
                                    </div>

                                    <div class="col-lg-4 col-sm-4 col-md-4">
                                        <strong>
                                            <label for="rg">RG
                                                <strong style="color: red;">*</strong>
                                            </label>
                                        </strong>
                                    <input type="text" name="rg" value="{{$func->rg}}" class="form-control"
                                        placeholder="0.000.000" 
                                        maxlength="9"
                                        onkeydown="javascript: fMasc( this, mRG );" required>

                                    </div>

                                </div>
                                <br>
                                <div class="row">

                                    
                                    <div class="col-lg-4 col-sm-4 col-md-4">
                                        <strong>
                                            <label for="matricula">Matrícula
                                                <strong style="color: red;">*</strong>
                                            </label>
                                        </strong>
                                        <input type="text" name="matricula" id="matricula" value="{{$func->matricula}}" class="form-control" required  pattern="^[0-9]+$" title="Digite somente Números">
                                    </div>

                                    <div class="col-lg-4 col-sm-4 col-md-4">
                                        <strong>
                                            <label for="perfil">Perfil
                                                <strong style="color: red;">*</strong>
                                            </label>
                                        </strong>
                                        <select name="perfil" value="{{$func->perfil}}" id="perfil" class="form-control" required>
                                            <option disabled value="">
                                                    Selecione
                                            </option>

                                            <optgroup label="Acesso ao Sistema">
                                               <option @if($func->perfil == 'Tarm') selected @endif value="Tarm">TARM</option>
                                               <option @if($func->perfil == 'Médico Regulador') selected @endif value="Médico Regulador">Médico Regulador</option>
                                               <option @if($func->perfil == 'Rádio Operador') selected @endif value="Rádio Operador">Rádio Operador</option>
                                               <option @if($func->perfil == 'Gestor') selected @endif value="Gestor">Gestor</option>
                                            </optgroup>

                                            <optgroup label="Sem Acesso ao Sistema">
                                               <option @if($func->perfil == 'Enfermeiro') selected @endif value="Enfermeiro">Enfermeiro</option>
                                               <option @if($func->perfil == 'Motorista') selected @endif value="Motorista">Motorista</option>
                                            </optgroup>
                                        </select>

                                     </div>

                                     <div class="col-lg-4 col-sm-4 col-md-4">
                                        <strong>
                                            <label for="nascimento">Data de Nascimento
                                                <strong style="color: red;">*</strong>
                                            </label>
                                        </strong>
                                        <input type="date" name="nascimento" value="{{$func->nascimento}}"id="nascimento" class="form-control" required>
                                    </div>

                                </div>
                                <br>
                                <div class="row">

                                    <div class="col-lg-2 col-sm-2 col-md-2">
                                        <strong>
                                            <label for="sexo">Sexo
                                                <strong style="color: red;">*</strong>
                                            </label>
                                        </strong>
                                        <select name="sexo" id="sexo" class="form-control" required>
                                             <option disabled value="">
                                                    Selecione
                                             </option>
                                             <option @if($func->sexo == "Masculino")selected @endif  value="Masculino">Masculino</option> 
                                             <option @if($func->sexo == "Feminino")selected @endif  value="Feminino">Feminino</option>
                                             <option @if($func->sexo == "Outros")selected @endif  value="Outros">Outros</option>
                                         </select>
                                     </div>

                                    <div class="col-lg-3 col-sm-3 col-md-3">
                                        <strong>
                                            <label for="contato">Contato
                                                <strong style="color: red;">*</strong>
                                            </label>
                                        </strong>
                                        <input type="text" name="contato" id="contato" value ="{{$func->contato}}"
                                        class="form-control"
                                        placeholder="(00)00000-0000" 
                                        maxlength="14"
                                        onkeydown="javascript: fMasc( this, mTel );"
                                        required>
                                        </div>

                                     <div class="col-lg-3 col-sm-3 col-md-3">
                                        <strong>
                                            <label for="">Outro Contato</label>
                                        </strong>
                                        
                                        <input type="text" name="contato2" id="contato2" value ="{{$func->contato2}}"
                                        class="form-control"
                                        placeholder="(00)0000-0000" 
                                        maxlength="14"
                                        onkeydown="javascript: fMasc( this, mTel );">
                                     </div>

                                      <div class="col-lg-4 col-sm-4 col-md-4">
                                        <strong>
                                            <label for="">E-Mail
                                                <strong style="color: red;">*</strong>
                                            </label>
                                        </strong>
                                        <input type="email" name="email" id="email" value ="{{$email}}" class="form-control" placeholder="example@example" required>
                                     </div>

                                </div>
                                <!-- FIM DO FORM CADASTRO DE FUNCIONARIOS-->
                                <br>
                                <h3>Endereço</h3>

                                <!--Linha da divisão-->
                                <li style="border-top: 2px #efefef solid; margin-top: 0px; margin-bottom: 0px; display: block;"></li>

                                <br>

                                <!-- FORM CADASTRO DE LOCAL FUNCIONARIOS-->

                                <div class="row">

                                    <div class="col-lg-2 col-sm-2 col-md-2">
                                        <strong>
                                            <label for="cep">CEP
                                                <strong style="color: red;">*</strong>
                                            </label>
                                        </strong>
                                        <input type="text" name="cep" id="cep" value="{{$local->cep}}" 
                                               class="form-control"
                                               placeholder="00000-000"
                                               maxlength="10"
                                               onkeydown="javascript: fMasc( this, mCEP );" required>
                                               <a href="http://www.buscacep.correios.com.br/sistemas/buscacep/" target="_blank"> Não sei meu CEP </a><br>

                                    </div>

                                    <div class="col-lg-4 col-sm-4 col-md-4">
                                        <strong>
                                            <label for="logradouro">Logradouro</label>
                                        </strong>
                                        <input type="text" name="logradouro" value="{{$local->logradouro}}"  id="logradouro" class="form-control"  readonly required>

                                    </div>


                                    <div class="col-lg-2 col-sm-2 col-md-2">
                                        <strong>
                                            <label for="numero">Número
                                                <strong style="color: red;">*</strong>
                                            </label>
                                        </strong>
                                        <input type="number" name="numero" value="{{$local->numero}}"  id="numero" class="form-control" required>
                                    </div>

                                    <div class="col-lg-3 col-sm-3 col-md-3">
                                        <strong>
                                            <label for="cidade">Cidade</label>
                                        </strong>
                                        <input type="text" name="cidade" id="cidade" value="{{$local->cidade}}"  class="form-control"  readonly required>
                                     </div>

                                     <div class="col-lg-1 col-sm-1 col-md-1">
                                        <strong>
                                            <label for="uf">Estado</label>
                                        </strong>
                                         <input type="text" name="uf" value="{{$local->uf}}"  id="uf" class="form-control"  readonly required>
                                     </div>



                                </div>
                                <br>

                                <div class="row">
                                    <div class="col-lg-6 col-sm-6 col-md-6">
                                        <strong>
                                            <label for="bairro">Bairro</label>
                                        </strong>
                                        <input type="text" name="bairro" id="bairro" value="{{$local->bairro}}" class="form-control"  readonly required>

                                    </div>
                                    <div class="col-lg-6 col-sm-6 col-md-6">
                                        <strong>
                                            <label for="referencia">Referência</label>
                                        </strong>
                                        <input type="text" name="referencia" id="referencia" value="{{$local->referencia}}"  class="form-control">

                                    </div>
                                </div>

                                <br>
                                <div>
                                    <div class="float-right">
                                        <input type="submit" name="Salvar" class="btn btn-primary" value="Salvar">
                                    </div> 
                                </div>
                            </form>
                            <div class="float-right" style="padding-right: 5px;">
                                <button class="btn btn-primary" onclick="window.location.href='/funcionario'"><a href="/funcionario" style="color: #fff; text-decoration: none;">Cancelar</a></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <br>
                <div class="card">
                    <div class="card-header"><h2> Frotas </h2></div>

                    <div class="card-body">
                        @if (Session::has('sucesso-frota'))
                            <div class="alert alert-success" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                  </button>
                                <i class="fas fa-check-circle"></i>
                                {{Session::pull('sucesso-frota')}}

                            </div>
                        @endif
                        @if (Session::has('falha-frota'))
                            <div class="alert alert-danger" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                </button>
                                <i class="fas fa-times-circle"></i>
                                {{Session::pull('falha-frota')}}
                            </div>
                        @endif

             <div id="accordionFilter">
                         <div class="mb-0">
                            <button class="btn btn-link bg-white" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" style="text-decoration: none;">
                            Filtros<i class="fas fa-filter btn-sm"></i>
                            </button>
                            <li style="border-top: 2px #efefef solid; display: block;"> </li>
                           
                        </div>
                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionFilter">
                        <div class="jumbotron jumbotron-fluid"
                             style="background-color: rgba(0,0,0,.03); box-shadow: 0px  0px 1px #000000;">

                            <div class="container">
                                <form action="{{route('frotabusca')}}" method="post">
                                    {{ csrf_field() }}
                                    <div class="row col-12" style="margin-top: -35px;">

                                        <div class="col-lg-3 col-sm-3 col-md-3">
                                            <strong>
                                                <label for="numero">Número</label>
                                            </strong>
                                            <input type="text" name="numero" id="numero" class="form-control" autofocus
                                             pattern="^[0-9]+$" title="Digite somente Números"><br>
                                        </div>

                                        <div class="col-lg-3 col-sm-3 col-md-3">
                                            <strong>
                                                <label for="placa">Placa</label>
                                            </strong>
                                            <input type="text" name="placa" id="placa" class="form-control" style="text-transform: uppercase;"
                                                maxlength="8"><br>
                                        </div>

                                        <div class="col-lg-3 col-sm-3 col-md-3">
                                            <strong>
                                                <label for="tipo">Tipo</label>
                                            </strong>
                                            <select name="tipo" id="tipo" class="form-control">
                                                <option disabled selected>Selecione</option>
                                                @foreach($tipos as $tipo)
                                                <option value="{{$tipo->id}}">{{$tipo->descricao}}</option>
                                                    @endforeach
                                            </select><br>
                                        </div>

                                        <div class="col-lg-3 col-sm-3 col-md-3">
                                            <strong>
                                                <label for="situacao">Situação</label>
                                            </strong>
                                            <select name="situacao" id="situacao" class="form-control">
                                                <option disabled selected>Selecione</option>
                                                <option value="1">Ativo</option>
                                                <option value="0">Inativo</option>
                                            </select>
                                        </div>
                                    </div>

                                    <br>
                                    <div class="container">

                                        <button type="submit" name="pesquisar" title="Pesquisar" class="btn btn-primary" value="Pesquisar">
                                        <i class="fas fa-search"></i>
                                          Pesquisar
                                          
                                        </button>

                                        <button type="reset" name="limpar" title="Limpar pesquisa" class="btn btn-primary" value="Limpar">
                                            <i class="fas fa-undo-alt"></i>
                                         Limpar 
                                          
                                        </button>



                                    </div>
                                </form>

                            </div>

                        </div>
                    </div>
                </div>
                <br>

                        <div class="container col-12">
                            <div class="row">

                                <div class="col-md-6 col-sm-6 col-lg-6">
                                    <h2 class="float-left"> Lista </h2>
                                </div>

                                <div class="col-md-6 col-sm-6 col-lg-6">

                                    <button type="button" class="btn btn-primary float-lg-right"
                                        onclick="window.location.href='/frota/novo'">
                                        <i class="fas fa-plus"></i>
                                        Nova Frota

                                        </button>

                                </div>

                            </div>
                        </div>

                        <li style="border-top: 2px #efefef solid; margin-top: 0px; margin-bottom: 0px; display: block;"></li>

                        <br>

                        <div class="table-responsive">
                            <table class="table table-hover table-bordered">
                                <thead style="background-color: rgba(0,0,0,.03); text-align: center;">
                                <tr>
                                    <th>Número</th>
                                    <th>Placa</th>
                                    <th>Tipo</th>
                                    <th>Situação</th>
                                    <th colspan="3" width="1%">Ações</th>
                                </tr>
                                </thead>

                                <tbody style="text-align: center;">
                                @foreach($frota as $ambulancia)
                                    <?php
                                    $t = \App\Tipo::findOrFail($ambulancia->tipo_id);
                                    ?>
                                <tr>
                                    <td>{{$ambulancia->numero}}</td>
                                    <td style="text-transform: uppercase;">
                                        {{$ambulancia->placa}}
                                    </td>
                                    <td>{{$t->descricao}}</td>
                                    @if($ambulancia->situacao == 1)
                                        <td>
                                        Ativo
                                        </td>
                                        <td>
                                            <a href="{{route('frotastatus', ['id'=>$ambulancia->id])}}" class="btn btn-danger btn-sm" title="desativar"
                                            onclick="return confirm('Tem certeza que deseja desativar {{$ambulancia->placa}}?');">
                                                <i class="fa fa-ban"></i>
                                                Desativar
                                            </a>
                                        </td>
                                    @else
                                        <td>
                                         Inativo
                                        </td>
                                        <td>
                                            <a href="{{route('frotastatus', ['id'=>$ambulancia->id])}}" class="btn btn-success btn-sm" title="ativar">
                                                <i class="fa fa-check"></i>
                                                Ativar
                                            </a>
                                        </td>
                                    @endif
                                    <td>
                                         <a href="{{route('frotaedit', ['id'=>$ambulancia->id])}}" class="btn btn-primary btn-sm" title="Editar"> 
                                      <i class="fas fa-edit"></i>
                                    </a>
                                    </td>
                                </tr>
                                    @endforeach
                                </tbody>

                            </table>
                        </div>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@extends('layouts.app')

@section('content')
<script type="text/javascript">
    function toggle(source) {
        var checkboxes = document.querySelectorAll('input[type="checkbox"]');
        for (var i = 0; i < checkboxes.length; i++) {
            if (checkboxes[i] != source){
                checkboxes[i].checked = source.checked;
            }
        }
    }

    function edit(nome, id){
        document.getElementById('inputEdit').value = nome;
        document.getElementById('idEdit').value = id;
    }
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});

</script>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
              <br>
                <div class="card">
                    <div class="card-header"> 
                        <h2>Especialidade </h2>
                        <br>
                        <div class="panel with-nav-tabs panel-primary">
                            <div class="panel-heading">
                                <ul class="nav nav-tabs" style="margin-bottom: -13px;">
                                    <li class="disabled nav-link">
                                        Estabelecimento

                                    </li>
                                    <li class="active nav-link">
                                        <a href="#" style="text-decoration: none; color: #212529;">
                                        Especialidade </a>
                                    </li>

                                </ul>
                            </div>
                        </div>


                    </div>

                    <div class="card-body">
                        <!-- Mensagem de Alerta-->
                        @if (Session::has(''))
                            <div class="alert alert-success" role="alert">
                                {{Session::pull('')}}
                            </div>
                        @endif


                        <div class="container col-12">
                          <div class="row">
                            <div class="col">
                              <h2> Buscar </h2>

                                <div class="input-group mb-3">
                                <div class="input-group-append">

                                  
                                     <span class="input-group-text">
                                       <i class="fas fa-search"></i>
                                     </span>
                                           
                                </div>
                                <input type="text" class="form-control" placeholder="Pesquisar Especialidade" aria-describedby="basic-addon2"
                                id="myInput">

                                
                                </div>
                              
                                 
                            </div>
                          </div>
                          <br>
                          <div class="row">
                              <div class="col-md-6 col-sm-6 col-lg-6">
                                  <h2> Lista </h2>
                              </div>

                              <div class="col-md-6 col-sm-6 col-lg-6">


                                <button class="btn btn-primary float-right" data-toggle="collapse" href="#multiCollapseExample1" role="button" aria-expanded="false" aria-controls="multiCollapseExample1">
                                    <i class="fas fa-plus"></i>
                                        Nova Especialidade
                                </button>

                              </div>
                          </div>
                                

                               
                          <div class="row">
                            <div class="col">
                                    
                              <div class="collapse multi-collapse"
                               id="multiCollapseExample1">
                                  <div class="card card-body">
                                      <form action="{{route('especsalvar')}}" method="post">
                                          {{ csrf_field() }}
                                          <strong>
                                              <label> Especialidade
                                              <strong style="color: red;">
                                                   * 
                                               </strong>
                                              </label>
                                           </strong>

                                           <input type="text" name="nome" id="nome" class="form-control" required>
                                           <br>
                                           <div>
                                              <div class="float-right">
                                                  <input type="submit" name="adicionar" class="btn btn-primary" value="Adicionar">
                                              </div>

                                              <div class="float-right" style="padding-right: 5px; ">

                                                <button class="btn btn-danger" onclick="window.location.href='/especialidade/{{$id}}/edit'">
                                                    <a href="/especialidade/{{$id}}/edit" style="color: #fff; text-decoration: none;">
                                                  Cancelar
                                                    </a>
                                                </button>
                                              </div>
                                          </div>
                                        </form>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                      <div class="collapse multi-collapse" id="multiCollapseExample2">
                                        <div class="card card-body">
                                          <form action="{{route('editname',[$id])}}" id='formEdit' method="post">
                                              {{ csrf_field() }}
                                          <strong>
                                              <label> Especialidade
                                                <strong style="color: red;">
                                                   * 
                                                 </strong>
                                              </label>
                                          </strong>
      
                                          <input type="text" name="nome" id='inputEdit' class="form-control" required>

                                          <input type="hidden" name="idEdit" id='idEdit' class="form-control" required>
                                          <br>
                                          <div>
                                          <div class="float-right">
                                                <input type="submit" name="" class="btn btn-primary" value="Editar">
                                          </div>

                                            <div class="float-right" style="padding-right: 5px;">
                                                <button class="btn btn-danger" onclick="window.location.href='/especialidade/{{$id}}/edit'">
                                                    <a href="/especialidade/{{$id}}/edit" style="color: #fff; text-decoration: none;">
                                                          Cancelar
                                                    </a>
                                                  </button> 
                                            </div>
                                          </div>
                                        </form>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                        <li style="border-top: 2px #efefef solid; margin-top: 0px; margin-bottom: 0px; display: block;">
                        </li>
                        <br>
                        <div class="table-responsive">
                          <form action="{{route('updatespec',['id'=> $id])}}" method="post">
                                {{ csrf_field() }}
                          <input type="hidden" value="{{$id}}" name="id" id="id">
                          
                          <table class="table table-hover table-bordered">
                                <thead style="background-color: rgba(0,0,0,.03); text-align: center;">
                                    <tr>
                                        <th>
                                         <input type="checkbox" onclick="toggle(this);" /> 
                                         Todos
                                         </th>
                                        <th>Especialidade</th>
                                        <th>Ação</th>
                                    </tr>
                                </thead>
                                @foreach ($espec as $especialidade)
                                <tbody style="text-align: center;" id="myTable">
                                

                                        <tr>
                                            <td>
                                            <input type="checkbox" @if($especialidade->status) checked @endif value="{{$especialidade->id}}" name="esp[]" />
                                            </td>
                                            <td>{{$especialidade->nome}}</td>
                                            <td>
                                                <a onclick="edit('{{$especialidade->nome}}','{{$especialidade->id}}')" class="btn btn-primary btn-sm" data-toggle="collapse" href="#multiCollapseExample2" role="button" aria-expanded="false" aria-controls="multiCollapseExample2">
                                                <i class="fas fa-edit"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    
                                 
                                </tbody>
                            @endforeach
                            </table>
                                <input type="submit" name="Salvar" class="btn btn-primary float-right" value="Salvar">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 col-sm-12 col-lg-12">
                <br>
                <div class="card">
                    <div class="card-header">
                        <h2>
                            Minha Conta
                        </h2>
                    </div>

                    <div class="card-body">

                        <div class="container col-12">

                            <h3> Dados do Usuário </h3>

                            <li style="border-top: 2px #efefef solid; margin-top: 0px; margin-bottom: 0px; display: block;"> </li>
                            <br>
                            <!-- FORM CADASTRO DE FUNCIONARIOS-->

                            <form method="POST" action="/meusdados/novo/save">
                                <div class="row">

                                    <div class="col-lg-4 col-sm-4 col-md-4">
                                        <strong>
                                            <label>Nome</label>
                                        </strong>
                                        <input type="text" name="nome" id="nome" class="form-control" value="{{$func->nome}}" disabled readonly>
                                    </div>

                                     <div class="col-lg-4 col-sm-4 col-md-4">
                                        <strong>
                                            <label for="cpf">
                                                CPF
                                            </label>
                                        </strong>
                                        <input type="text" name="cpf" class="form-control"
                                         value="{{$func->cpf}}" disabled readonly><br>
                                    </div>

                                    <div class="col-lg-4 col-sm-4 col-md-4">
                                        <strong>
                                            <label>RG</label>
                                        </strong>
                                        <input type="text" name="rg" id="rg" class="form-control" placeholder="0.000.000" value="{{$func->rg}}" disabled readonly>
                                    </div>

                                </div>
                           
                                <div class="row">

                                    <div class="col-lg-4 col-sm-4 col-md-4">
                                        <strong>
                                            <label>Matrícula</label>
                                        </strong>
                                        <input type="text" name="matricula" id="matricula" class="form-control" value="{{$func->matricula}}" disabled readonly>
                                    </div>

                                    <div class="col-lg-4 col-sm-4 col-md-4">
                                        <strong>
                                            <label>Perfil</label>
                                        </strong>
                                        <select name="perfil" id="perfil" class="form-control" disabled readonly>
                                            <option disabled selected>Selecione</option>

                                            <optgroup label="Acesso ao Sistema">
                                                <option @if($func->perfil == 'Tarm') selected @endif value="Tarm">TARM</option>
                                                <option @if($func->perfil == 'Médico Regulador') selected @endif value="Médico Regulador">Médico Regulador</option>
                                                <option @if($func->perfil == 'Rádio Operador') selected @endif value="Rádio Operador">Rádio Operador</option>
                                                <option @if($func->perfil == 'Gestor') selected @endif value="Gestor">Gestor</option>
                                            </optgroup>

                                            <optgroup label="Sem Acesso ao Sistema">
                                                <option @if($func->perfil == 'Enfermeiro') selected @endif value="Enfermeiro">Enfermeiro</option>
                                                <option @if($func->perfil == 'Motorista') selected @endif value="Motorista">Motorista</option>
                                            </optgroup>
                                        </select>
                                    </div>

                                    <div class="col-lg-4 col-sm-4 col-md-4">
                                        <strong>
                                            <label>Data de Nascimento</label>
                                        </strong>
                                        <input type="date" name="nascimento" value="{{$func->nascimento}}" id="nascimento" class="form-control" disabled readonly>
                                    </div>

                                </div>
                                <br>
                                <div class="row">

                                     <div class="col-lg-2 col-sm-2 col-md-2">
                                        <strong>
                                            <label>Sexo</label>
                                        </strong>
                                        <input type="text" value="{{$func->sexo}}"id="sexo" class="form-control" disabled readonly>
                                    </div>

                                    <div class="col-lg-3 col-sm-3 col-md-3">
                                        <strong>
                                            <label>Contato
                                               
                                            </label>
                                        </strong>
                                        <input type="text" name="contato" id="contato" value="{{$func->contato}}" class="form-control" placeholder="(00)00000-0000" disabled readonly>
                                    </div>

                                    <div class="col-lg-3 col-sm-3 col-md-3">
                                        <strong>
                                            <label>Outro Contato</label>
                                        </strong>
                                        <input type="text" name="" id="" value="{{$func->contato2}}" class="form-control" placeholder="(00)00000-0000" disabled readonly>
                                    </div>

                                    <div class="col-lg-4 col-sm-4 col-md-4">
                                        <strong>
                                            <label for="email">
                                                E-Mail
                                            </label>
                                        </strong>
                                        <input type="email" name="email" id="email" class="form-control" value="{{$func->email}}" disabled readonly><br>
                                     </div>

                                </div>
                            </form>

                        </div>

                        <!-- FIM DO FORM CADASTRO DE FUNCIONARIOS-->

                        <!-- FORM CADASTRO DE LOCAL FUNCIONARIOS-->
                        <?php
                            $local = \App\Local::findOrFail($func->idLocal);
                        ?>
                        <div class="container col-12">
                        <form method="POST" action="/funcionario/save">

                            <br>
                            <h3> Endereço </h3>
                             <li style="border-top: 2px #efefef solid; margin-top: 0px; margin-bottom: 0px; display: block;"> </li>
                             <br>
                            <div class="row">

                                <div class="col-lg-2 col-sm-2 col-md-2">
                                    <strong>
                                        <label for="cep">CEP  </label>
                                    </strong>
                                    <input type="text" name="cep" id="cep"
                                    class="form-control"
                                    placeholder="00000-000" 
                                    maxlength="10"
                                    onkeydown="javascript: fMasc( this, mCEP );"
                                           value="{{$local->cep}}"
                                    disabled readonly>

                                    
                                </div>

                                <div class="col-lg-4 col-sm-4 col-md-4">
                                    <strong>
                                        <label for="logradouro">Logradouro</label>
                                    </strong>
                                    <input type="text" name="logradouro" id="logradouro" value="{{$local->logradouro}}" class="form-control" disabled readonly>

                                </div>

                                <div class="col-lg-2 col-sm-2 col-md-2">
                                    <strong>
                                        <label for="numero">Número
                                            
                                        </label>
                                    </strong>
                                    <input type="number" name="numero" id="numero" value="{{$local->numero}}" class="form-control" disabled readonly>
                                </div>

                            <div class="col-lg-3 col-sm-3 col-md-3">
                                <strong>
                                    <label for="cidade">Cidade</label>
                                </strong>
                                <input type="text" name="cidade" id="cidade" value="{{$local->cidade}}" class="form-control" disabled readonly>
                            </div>

                            <div class="col-lg-1 col-sm-1 col-md-1">
                                <strong>
                                    <label for="uf">Estado</label>
                                </strong>
                                <input type="text" name="uf" id="uf" class="form-control" value="{{$local->uf}}" disabled readonly>
                            </div>
                        </div>
                            <br>
                            <div class="row">
                                 <div class="col-lg-6 col-sm-6 col-md-6">
                                    <strong>
                                        <label for="bairro">Bairro</label>
                                    </strong>
                                    <input type="text" name="bairro" id="bairro" class="form-control" value="{{$local->bairro}}" disabled readonly>

                                </div>
                                <div class="col-lg-6 col-sm-6 col-md-6">
                                    <strong>
                                        <label for="referencia">Referência</label>
                                    </strong>
                                    <input type="text" name="referencia" id="referencia" value="{{$local->referencia}}" class="form-control" disabled readonly>

                                </div>
                            </div>
                        </form>
                        <br>
                        <div>
                            <div class="float-right" style="padding-right: 5px;">
                                <button class="btn btn-primary" onclick="window.location.href='/novasenha'"><a href="/novasenha" style="color: #fff; text-decoration: none;">Alterar Senha</a></button>
                            </div>

                            <div class="float-right" style="padding-right: 5px;">
                                <button class="btn btn-primary" onclick="window.location.href='/funcionario/{{$func->id}}/edit'"><a href="/funcionario/{{$func->id}}/edit" style="color: #fff; text-decoration: none;">Editar</a></button>
                            </div>

                        </div>
                    </div>
                        <br>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
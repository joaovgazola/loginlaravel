@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 col-sm-12 col-lg-12">
                <br>
                <div class="card">
                    <div class="card-header">
                        <h2>
                            Alterar Senha
                        </h2>
                    </div>
                   
                    <div class="card-body">

                    <div class="alert alert-info" role="alert">
                            Os campos marcados com <strong> * </strong> são de preenchimento obrigatório.
                    </div>
                    <div class="alert alert-warning" role="alert">
                            <b>NOTA:</b> Caso a Senha do usário logado seja alterada o usuário será redirecionado para tela de Login.
                            <i class="fas fa-exclamation-triangle"></i>
                        </div>

                    <div class="container col-12">

                        @if (Session::has('erro'))
                            <div class="alert alert-danger" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                 <span aria-hidden="true">&times;</span>
                                </button>
                                <i class="fas fa-times-circle"></i>
                                {{Session::pull('erro')}}
                            </div>
                        @endif
                        <h3> Nova Senha</h3>
                        <li style="border-top: 2px #efefef solid; margin-top: 0px; margin-bottom: 0px; display: block;"> </li>
                        <br>
                    <form action="{{route('salvarsenha')}}" method="post">
                        {{ csrf_field() }}
                        <div class="row">

                            <div class="col-lg-3 col-sm-3 col-md-3">
                                <strong>
                                    <label>Usuário</label>
                                </strong>
                                <input value="{{$username}}" type="text" name="cpf" id="cpf" class="form-control" placeholder="000.000.000-00" disabled readonly>
                            </div>

                            <div class="col-lg-3 col-sm-3 col-md-3">
                                <strong>
                                    <label>Senha Atual
                                        <strong style="color: red;">*</strong>
                                    </label>
                                </strong>

                                <div class="input-group">

                                    <input type="password" class="form-control" 
                                    id="senhaAtual" name="senhaAtual" class="form-control"required>

                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">
                                        <i class="fa fa-eye"
                                         onclick="togglerAtual(this);"></i>
                                        </span>
                                    </div>
                                </div>

                            </div>

                            <div class="col-lg-3 col-sm-3 col-md-3">
                                <strong>
                                    <label>Nova Senha
                                        <strong style="color: red;">*</strong>
                                    </label>
                                </strong>

                                <div class="input-group">
                                            
                                    <input type="password" minlength="8" class="form-control" id="novaSenha" name="novaSenha" class="form-control" required>
                                          

                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">
                                            <i class="fa fa-eye"
                                             onclick="togglerNova(this);"></i>
                                        </span>
                                    </div>

                                </div>

                            </div>

                            <div class="col-lg-3 col-sm-3 col-md-3">
                                <strong>
                                    <label>Confirmar a Senha
                                        <strong style="color: red;">*</strong>
                                    </label>
                                </strong>

                                <div class="input-group">
                                            
                                    <input type="password" minlength="8" class="form-control" id="confirm" name="confirm" class="form-control" required>

                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">
                                            <i class="fa fa-eye"
                                             onclick="togglerConfirma(this);"></i>
                                        </span>
                                    </div>
                                            
                                </div>

                            </div>

                        </div>
                        <br>
                        <br>
                            <div class="float-right">
                                        <!--<button class="btn btn-primary" onclick="window.location.href='/senha/salvar'"><a href="/senha/salvar" style="color: #fff; text-decoration: none;">Salvar</a></button> -->
                                <input type="submit" name="salvar" class="btn btn-primary" title="Salvar" value="Salvar">
                            </div>
                    </form>
                        <div class="float-right" style="padding-right: 5px;">
                            <button class="btn btn-primary" onclick="window.location.href='/meusdados'"><a href="/meusdados" style="color: #fff; text-decoration: none;">Cancelar</a></button>
                        </div>
                        
                    </div>
                    <br>
                </div>
                </div>
            </div>
        <div>
    </div>
@endsection
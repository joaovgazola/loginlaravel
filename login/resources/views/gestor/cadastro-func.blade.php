@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 col-sm-12 col-lg-12">
                <br>
                <div class="card">
                    <div class="card-header">
	                     <h2> 
	                        Cadastro de Funcionário 
	                    </h2>

                    </div>
        
                    <div class="card-body">
                        @if (Session::has('erro-cadastro'))
                            <div class="alert alert-danger" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                {{Session::pull('erro-cadastro')}}
                            </div>
                        @endif

                        <div class="alert alert-info" role="alert">
                            Os campos marcados com <strong> * </strong> são de preenchimento obrigatório.
                        </div>

                    <div class="container col-12">

                        <h3> Dados do Funcionário </h3>

                        <li style="border-top: 2px #efefef solid; margin-top: 0px; margin-bottom: 0px; display: block;"> </li>
                        <br>
                        <!-- FORM CADASTRO DE FUNCIONARIOS-->
                        
                            <form method="post" action="{{route('funcsalvar')}}">
                                {{ csrf_field() }}
                                <div class="row">

                                    <div class="col-lg-4 col-sm-4 col-md-4">
                                        <strong>
                                            <label for="nome">Nome
                                                <strong style="color: red;">*</strong>
                                            </label>
                                        </strong>
                                        <input type="text" @isset($func) value="{{$func->nome}}" @endisset name="nome" id="nome" class="form-control" autofocus required pattern="[a-z A-Z À-ú]+$" title="Digite somente Letras"> <br>
                                    </div>

                                     <div class="col-lg-4 col-sm-4 col-md-4">
                                        <strong>
                                            <label for="cpf">CPF
                                                <strong style="color: red;">*</strong>
                                            </label>
                                        </strong>
                                        <input type="text" name="cpf" class="form-control" @isset($func) value="{{$func->cpf}}" @endisset
                                        placeholder="000.000.000-00" 
                                        maxlength="14"
                                        onkeydown="javascript: fMasc( this, mCPF );" required><br>
                                    </div>

                                    <div class="col-lg-4 col-sm-4 col-md-4">
                                        <strong>
                                            <label for="rg">RG
                                                <strong style="color: red;">*</strong>
                                            </label>
                                        </strong>
                                    <input type="text" name="rg" class="form-control" @isset($func) value="{{$func->rg}}" @endisset
                                        onkeydown="javascript: fMasc( this, mRG );" required><br>

                                    </div>

                                </div>
                                
                                <div class="row">

                                    
                                    <div class="col-lg-4 col-sm-4 col-md-4">
                                        <strong>
                                            <label for="matricula">Matrícula
                                                <strong style="color: red;">*</strong>
                                            </label>
                                        </strong>
                                        <input type="text" name="matricula" maxlength="10" id="matricula" class="form-control" required  @isset($func) value="{{$func->matricula}}" @endisset
                                        pattern="^[0-9]+$" title="Digite somente Números"><br>
                                    </div>

                                    <div class="col-lg-4 col-sm-4 col-md-4">
                                        <strong>
                                            <label for="perfil">Perfil
                                                <strong style="color: red;">*</strong>
                                            </label>
                                        </strong>
                                        <select name="perfil" id="perfil" class="form-control" required>
                                            <option @empty($func) selected @endempty disabled value="">
                                                    Selecione
                                            </option>

                                            <optgroup label="Acesso ao Sistema">
                                                <option @isset($func)  @if($func->perfil == "Tarm") selected @endif @endisset value="Tarm">TARM</option>
                                                <option @isset($func)  @if($func->perfil == "Médico Regulador") selected @endif @endisset value="Médico Regulador">Médico Regulador</option>
                                                <option @isset($func)  @if($func->perfil == "Rádio Operador") selected @endif @endisset value="Rádio Operador">Radio Operador</option>
                                                <option @isset($func)  @if($func->perfil == "Gestor") selected @endif @endisset value="Gestor">Gestor</option>
                                            </optgroup>

                                            <optgroup label="Sem Acesso ao Sistema">
                                                <option @isset($func)  @if($func->perfil == "Enfermeiro") selected @endif @endisset value="Enfermeiro">Enfermeiro</option>
                                                <option @isset($func)  @if($func->perfil == "Motorista") selected @endif @endisset value="Motorista">Motorista</option>
                                            </optgroup>
                                        </select><br>

                                     </div>

                                     <div class="col-lg-4 col-sm-4 col-md-4">
                                        <strong>
                                            <label for="nascimento">Data de Nascimento
                                                <strong style="color: red;">*</strong>
                                            </label>
                                        </strong>
                                        <input type="date" name="nascimento" max="{{date('d').'-'.date('m').'-'.date('Y')}}" id="nascimento" class="form-control" required  @isset($func) value="{{$func->nascimento}}" @endisset><br> 
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col-lg-2 col-sm-2 col-md-2">
                                        <strong>
                                            <label for="sexo">Sexo
                                                <strong style="color: red;">*</strong>
                                            </label>
                                        </strong>
                                        <select name="sexo" id="sexo" class="form-control" required>
                                             <option @empty($func) selected @endempty disabled value="">
                                                    Selecione
                                             </option>
                                             <option @isset($func)  @if($func->sexo == "Masculino") selected @endif @endisset value="Masculino">Masculino</option> 
                                             <option @isset($func)  @if($func->sexo == "Feminino") selected @endif @endisset value="Feminino">Feminino</option>
                                             <option @isset($func)  @if($func->sexo == "Outros") selected @endif @endisset value="Outros">Outros</option>
                                         </select><br>
                                     </div>

                                    <div class="col-lg-3 col-sm-3 col-md-3">
                                        <strong>
                                            <label for="contato">Contato
                                                <strong style="color: red;">*</strong>
                                            </label>
                                        </strong>
                                        <input type="text" name="contato" id="contato" @isset($func) value="{{$func->contato}}" @endisset
                                        class="form-control"
                                        placeholder="(00)00000-0000" 
                                        maxlength="14"
                                        onkeydown="javascript: fMasc( this, mTel );"
                                        required><br>
                                        </div>

                                     <div class="col-lg-3 col-sm-3 col-md-3">
                                        <strong>
                                            <label for="">Outro Contato</label>
                                        </strong>
                                        
                                        <input type="text" name="contato2" id="contato2" @isset($func) value="{{$func->contato2}}" @endisset
                                        class="form-control"
                                        placeholder="(00)0000-0000" 
                                        maxlength="14"
                                        onkeydown="javascript: fMasc( this, mTel );"><br>
                                     </div>

                                      <div class="col-lg-4 col-sm-4 col-md-4">
                                        <strong>
                                            <label for="">E-Mail
                                                <strong style="color: red;">*</strong>
                                            </label>
                                        </strong>
                                        <input type="email" name="email" id="email" class="form-control" placeholder="example@example" @isset($func) value="{{$func->email}}" @endisset required><br>
                                     </div>

                                </div>
                                <!-- FIM DO FORM CADASTRO DE FUNCIONARIOS-->
                              
                                <h3>Endereço</h3>

                                <!--Linha da divisão-->
                                <li style="border-top: 2px #efefef solid; margin-top: 0px; margin-bottom: 0px; display: block;"></li>

                                <br>

                                <!-- FORM CADASTRO DE LOCAL FUNCIONARIOS-->

                                <div class="row">

                                    <div class="col-lg-2 col-sm-2 col-md-2">
                                        <strong>
                                            <label for="cep">CEP
                                                <strong style="color: red;">*</strong>
                                            </label>
                                        </strong>
                                        <input type="text" name="cep" id="cep" @isset($func) value="{{$func->cep}}" @endisset
                                               class="form-control"
                                               placeholder="00000-000"
                                               maxlength="10"
                                               onkeydown="javascript: fMasc( this, mCEP );" required>
                                               <a href="http://www.buscacep.correios.com.br/sistemas/buscacep/" target="_blank"> Não sei meu CEP </a><br><br>
                                    </div>

                                    <div class="col-lg-4 col-sm-4 col-md-4">
                                        <strong>
                                            <label for="logradouro">Logradouro</label>
                                        </strong>
                                        <input type="text" name="logradouro" id="logradouro" class="form-control"  readonly required @isset($func) value="{{$func->logradouro}}" @endisset><br>

                                    </div>


                                    <div class="col-lg-2 col-sm-2 col-md-2">
                                        <strong>
                                            <label for="numero">Número
                                                <strong style="color: red;">*</strong>
                                            </label>
                                        </strong>
                                        <input type="number" name="numero" id="numero" class="form-control" required @isset($func) value="{{$func->numero}}" @endisset><br>
                                    </div>

                                    <div class="col-lg-3 col-sm-3 col-md-3">
                                        <strong>
                                            <label for="cidade">Cidade</label>
                                        </strong>
                                        <input type="text" name="cidade" id="cidade" class="form-control"  readonly required @isset($func) value="{{$func->cidade}}" @endisset><br>
                                     </div>

                                     <div class="col-lg-1 col-sm-1 col-md-1">
                                        <strong>
                                            <label for="uf">Estado</label>
                                        </strong>
                                         <input type="text" name="uf" id="uf" class="form-control"  readonly required @isset($func) value="{{$func->uf}}" @endisset><br>
                                     </div>

                                </div>
                              
                                <div class="row">
                                    <div class="col-lg-6 col-sm-6 col-md-6">
                                        <strong>
                                            <label for="bairro">Bairro</label>
                                        </strong>
                                        <input type="text" name="bairro" id="bairro" class="form-control"  readonly required @isset($func) value="{{$func->bairro}}" @endisset><br>
                                    </div>
                                    <div class="col-lg-6 col-sm-6 col-md-6">
                                        <strong>
                                            <label for="referencia">Referência</label>
                                        </strong>
                                        <input type="text" name="referencia" id="referencia" class="form-control" @isset($func) value="{{$func->referencia}}" @endisset>

                                    </div>
                                </div>

                                <br>
                                <div>
                                    <div class="float-right">
                                        <input type="submit" name="cadastro" class="btn btn-primary" value="Cadastrar">
                                    </div>
                                    
                                </div>
                            </form>
                            <div class="float-right" style="padding-right: 5px;">
                                        <button class="btn btn-primary" onclick="window.location.href='/funcionario'"><a href="/funcionario" style="color: #fff; text-decoration: none;">Cancelar</a></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
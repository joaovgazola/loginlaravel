@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <br>
                <div class="card">
                    <div class="card-header"> <h2> Estabelecimentos </h2>
                    </div>

                    <div class="card-body">
                        <!-- Mensagem de Alerta status-->
                        @if (Session::has('sucesso-estabelecimento'))
                            <div class="alert alert-success" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                 <span aria-hidden="true">&times;</span>
                                </button>
                                <i class="fas fa-check-circle"></i>
                                {{Session::pull('sucesso-estabelecimento')}}

                            </div>
                        @endif
                        @if (Session::has('falha-estabelecimento'))
                            <div class="alert alert-danger" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                 </button>
                                <i class="fas fa-times-circle"></i>
                                {{Session::pull('falha-estabelecimento')}}
                            </div>
                        @endif

                        <!--FIM Mensagem de Alerta status-->


                        <!-- Mensagem de Alerta novo estabelecimento-->
                        @if (Session::has('novo-estab'))
                            <div class="alert alert-success" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                 <span aria-hidden="true">&times;</span>
                                </button>
                                <i class="fas fa-check-circle"></i>
                                {{Session::pull('novo-estab')}}

                            </div>
                        @endif
                         <!--FIM  Mensagem de Alerta novo estabelecimento-->

                        <!-- Mensagem de Alerta edit estabelecimento-->
                        @if (Session::has('edit-estab'))
                            <div class="alert alert-success" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                 <span aria-hidden="true">&times;</span>
                                </button>
                                <i class="fas fa-check-circle"></i>
                                {{Session::pull('edit-estab')}}

                            </div>
                        @endif
                        <!-- Fim da Mensagem de Alerta estabelecimento -->

                        
                <div id="accordionFilter">
                        <div class="mb-0">
                            <button class="btn btn-link bg-white" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" style="text-decoration: none;">
                            Filtros<i class="fas fa-filter btn-sm"></i>
                            </button>
                            <li style="border-top: 2px #efefef solid; display: block;"> </li>
                           
                        </div>
                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionFilter">
                        <div class="jumbotron jumbotron-fluid"
                             style="background-color: rgba(0,0,0,.03); box-shadow: 0px  0px 1px #000000;">
                            <div class="container">
                                    <form action="{{route('estabelecimentobusca')}}" method="post">
                                        {{ csrf_field() }}
                                    <div class="row col-12" style="margin-top: -35px;">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <strong><label> Nome</label></strong>
                                                <input type="text" name="nome" class="form-control" autofocus><br>

                                        </div>

                                        <div class="col-lg-5 col-md-5 col-sm-5">
                                            <strong><label> Especialidade</label></strong>
                                            <select name="especialidade" id="especialidade" class="form-control">
                                                <option disabled selected value="null">Selecione</option>
                                                @foreach($especs as $espec)
                                                    <option value="{{$espec->id}}">{{$espec->nome}}</option>
                                                @endforeach
                                            </select><br>
                                        </div>

                                         <div class="col-lg-3 col-md-3 col-sm-3">
                                            <strong><label> Situação</label></strong>
                                            <select name="situacao" id="situacao" class="form-control">
                                                <option disabled selected value="null">Selecione</option>
                                                <option value="1">Ativo</option>
                                                <option value="0">Inativo</option>
                                            </select>
                                        </div>
                                    </div>
                                    <!--Button-->
                                    <br>
                                    <div class="container">

                                        <button type="submit" name="pesquisar" title="Pesquisar" class="btn btn-primary" value="Pesquisar">
                                        <i class="fas fa-search"></i>
                                        Pesquisar
                                          
                                        </button>

                                        <button type="reset" name="limpar" title="Limpar pesquisa" class="btn btn-primary" value="Limpar">
                                            <i class="fas fa-undo-alt"></i>
                                         Limpar 
                                          
                                        </button>
          
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <br>

                        <!--Lista / Novo Registro-->
                        <div class="container col-12">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-lg-6">
                                        <h2> Lista </h2>
                                    </div>

                                    <div class="col-md-6 col-sm-6 col-lg-6">
                                        <button type="button" class="btn btn-primary float-lg-right"
                                        onclick="window.location.href='/estabelecimento/novo'">
                                        <i class="fas fa-plus"></i>
                                        Novo Estabelecimento
                                        
                                        </button>
                                    </div>
                                </div>
                        </div>


                        <li style="border-top: 2px #efefef solid; margin-top: 0px; margin-bottom: 0px; display: block;">
                        </li>
                        <br>

                        <div class="table-responsive">
                            <table class="table table-hover table-bordered">
                                <thead style="background-color: rgba(0,0,0,.03); text-align: center;">
                                <tr>
                                    <th>Nome</th>
                                    <th>Especialidade</th>
                                    <th>Situação</th>
                                    <th colspan="3" width="1%">Ações</th>


                                </tr>
                                </thead>
                                <!--- Tabela Aguardando --->

                                <tbody style="text-align: center;">
                                <?php
                                foreach($estab as $estabelecimento){
                                    ?>
                                <tr>
                                    <td>{{ $estabelecimento->nome}}</td>
                                    <td>
                                        <?php
                                            $e = \App\Esptoestab::query();
                                            $e = $e->where('idEstab', $estabelecimento->id)->get();
                                            foreach($e as $esp){
                                                $especialidade = \App\Especialidade::findOrFail($esp->idEsp);

                                                echo "<small><b> <li style='display: inline-block;  text-transform: uppercase;'>".$especialidade->nome. ". </li></b></small> ";
                                            }
                                        ?>
                                    </td>
                                    @if($estabelecimento->status == 1)
                                    <td>
                                        Ativo
                                    </td>
                                        <td>
                                            <a href="{{route('estabstatus', ['id'=>$estabelecimento->id])}}" class="btn btn-danger btn-sm" title="Desativar"
                                            onclick="return confirm('Tem certeza que deseja desativar {{$estabelecimento->nome}}?');">
                                                <i class="fa fa-ban"></i>
                                                Desativar
                                            </a>
                                        </td>
                                    @else
                                    <td>
                                        Inativo
                                    </td>
                                        <td>
                                            <a href="{{route('estabstatus', ['id'=>$estabelecimento->id])}}" class="btn btn-success btn-sm" title="Ativar">
                                                <i class="fa fa-check"></i>
                                                Ativar
                                            </a>
                                        </td>
                                    @endif
                                    <td>
                                        
                                        <a href="{{route('estabEdit', ['id'=>$estabelecimento->id])}}" class="btn btn-primary btn-sm" title="Editar"> 
                                        <i class="fas fa-edit"></i>
                                        </a>
                                    </td>
                                        

                                </tr>
                                <?php
                                        }
                                ?>
                                </tbody>


                            </table>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@extends('layouts.app')

@section('content')
<!-- variaveis para chamar os campos do dash -->
    <?php
    use App\Func;
    use App\Estabelecimento;
    use App\Especialidade;
    use App\Frota;
    use App\Tipo;
    $funcA = Func::query();
    $funcA = $funcA->where('status', true)->get();
    $funcI = Func::query();
    $funcI = $funcI->where('status', false)->get();
    $funcAc = Func::query();
    $funcAc = $funcAc->whereIn('perfil', ['Tarm', 'Médico Regulador', 'Rádio Operador', 'Gestor'])->get();
    $estabA = Estabelecimento::query();
    $estabA = $estabA->where('status', true)->get();
    $estabI = Estabelecimento::query();
    $estabI = $estabI->where('status', false)->get();
    $espec = Especialidade::all();
    $frotaA = Frota::query();
    $frotaA = $frotaA->where('situacao', true)->get();
    $frotaI = Frota::query();
    $frotaI = $frotaI->where('situacao', false)->get();
    $tipos = Tipo::all();
    ?>

<!-- fim -->

<!-- timer
<script type="text/javascript">
      var min    = 0;
      var second = 00;
      var zeroPlaceholder = 0;
      var counterId = setInterval(function(){
                        countUp();
                      }, 1000);

      function countUp () {
          second++;
          if(second == 59){
            second = 00;
            min = min + 1;
          }
          if(second == 10){
              zeroPlaceholder = '';
          }else
          if(second == 00){
              zeroPlaceholder = 0;
          }

          document.getElementById("count-up").innerText = min+':'+zeroPlaceholder+second;
      }

</script>
-->

<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-12">
            <br>
            <div class="card" style="background-color: #fff;">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                  <!-- <h3> <span id="count-up" style="color: red;">0:00</span> </h3> -->
                    <h2 style="color: #333;"> 
                       Dashboard 
                    </h2> 

                    <li style="border-top: 2px #efefef solid; margin-top: 0px; margin-bottom: 0px; display: block;"> </li>
                    <br>

                        <!-- DASH COM A QUANTIDADE TOTAL  -->
                        <div class="row col-12">

                            <div class="col-md-4">
                              <div class="card bg-primary" style="box-shadow: 3px 3px 3px;">
                                 <div class="card-header text-white bg-primary">

                                   <i class="fas fa-users"></i> Funcionários Total

                                 </div>
                                        <div class="card-body bg-white">

                                            <div class="row col-12">
                                            <h1 class="card-title text-primary">{{count($funcA)+count($funcI)}}</h1>
                                             <strong class="text-primary" style="padding: 15px 6px;"> Qtde</strong>
                                            <p class="card-text" style="text-align: center">Número total de Funcionários.</p>
                                            </div>   
                                        </div>
                              </div>
                              <br>
                            </div>

                            <div class="col-md-4">
                              <div class="card bg-primary" style="box-shadow: 3px 3px 3px;">
                                 <div class="card-header text-white bg-primary">

                                   <i class="fas fa-ambulance"></i> Frota Total

                                 </div>
                                        <div class="card-body bg-white">

                                            <div class="row col-12">
                                            <h1 class="card-title text-primary">{{count($frotaA)+count($frotaI)}}</h1>
                                             <strong class="text-primary" style="padding: 15px 6px;"> Qtde</strong>
                                            
                                            </div>
                                            <p class="card-text">
                                            Número total de Frotas.</p>
                                        </div>
                              </div>
                              <br>
                            </div>

                            <div class="col-md-4">
                              <div class="card bg-primary" style="box-shadow: 3px 3px 3px;">
                                 <div class="card-header text-white bg-primary">

                                   <i class="fas fa-hospital"></i> Estabelecimento Total

                                 </div>
                                        <div class="card-body bg-white">

                                            <div class="row col-12">
                                            <h1 class="card-title text-primary">{{count($estabA)+count($estabI)}}</h1>
                                             <strong class="text-primary" style="padding: 15px 6px;"> Qtde</strong>
                                            <p class="card-text" style="text-align: center">Número total de Estabelecimento.</p>
                                            </div>   
                                        </div>
                              </div>

                          </div>  
                      </div>

                        <!-- FIM DASH TOTAL -->
                        <br>
                        <br>

             <!-- DASH DOS CAMPOS DE FUNCIONARIO -->           
            <div id="accordion1">
                <div class="acord1">
                    <div class="" id="headingOne">
                      <div class="mb-0">
                        <button class="btn bg-white dropdown-toggle float-right" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        </button>
                        <h4>
                           Funcionários 
                        </h4>

                        <li style="border-top: 2px #efefef solid; margin-top: 0px; margin-bottom: 0px; display: block;"> </li>
                                <br>
                        
                        
                      </div>
                    </div>



                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion1">
                      <div class="">
                        <div class="row col-lg-12">
                                <div class="col-md-3">
                                  <div class="card bg-success" style="box-shadow: 3px 3px 3px;">
                                     <div class="card-header text-white bg-success">

                                        <i class="fas fa-users"></i> Funcionários Ativos

                                     </div>
                                            <div class="card-body bg-white">

                                                <div class="row col-lg-12">
                                                <h1 class="card-title text-success">{{count($funcA)}}</h1>
                                                 <strong class="text-success" style="padding: 15px 6px;"> Qtde</strong>
                                                <p class="card-text" style="text-align: center">Funcionários Ativos.</p>
                                                </div>   
                                            </div>
                                  </div>
                                  <br>  
                                </div>


                                 <div class="col-md-3">
                                  <div class="card bg-danger" style="box-shadow: 3px 3px 3px;">
                                     <div class="card-header text-white bg-danger">

                                        <i class="fas fa-users"></i> Funcionários Inativos

                                     </div>
                                            <div class="card-body bg-white">

                                                <div class="row col-lg-12">
                                                <h1 class="card-title text-danger">{{count($funcI)}}</h1>
                                                 <strong class="text-danger" style="padding: 15px 6px;"> Qtde</strong>
                                                <p class="card-text" style="text-align: center">Funcionários Inativos.</p>
                                                </div>   
                                            </div>
                                  </div>
                                  <br>
                                 </div>


                            <div class="col-md-3">
                              <div class="card bg-info" style="box-shadow: 3px 3px 3px;">
                                 <div class="card-header text-white bg-info">

                                   <i class="fas fa-lock-open"></i>  Funcionários Com Acesso 

                                 </div>
                                        <div class="card-body bg-white">

                                            <div class="row">
                                            <h1 class="card-title text-info">{{count($funcAc)}}</h1>
                                             <strong class="text-info" style="padding: 15px 6px;"> Qtde</strong>
                                            <p class="card-text" style="text-align: center">Funcionários Com Acesso.</p>
                                            </div>   
                                        </div>
                                </div>
                                <br>
                            </div>


                            <div class="col-md-3">
                              <div class="card bg-light" style="box-shadow: 3px 3px 3px;">
                                 <div class="card-header text-dark bg-light">

                                   <i class="fas fa-lock"></i> Funcionários Sem Acesso 

                                 </div>
                                        <div class="card-body bg-white">

                                            <div class="row">
                                            <h1 class="card-title text-dark">{{(count($funcA)+count($funcI))-count($funcAc)}}</h1>
                                             <strong class="text-dark" style="padding: 15px 6px;"> Qtde</strong>
                                            <p class="card-text" style="text-align: center">Funcionários Sem Acesso.</p>
                                            </div>   
                                        </div>
                                </div>
                                  <br>
                            </div>
                        </div> 
                    </div>
                </div>
              </div>
            </div>
            <!-- FIM DO DASH DOS FUNCIONARIOS -->
            <br>

            <!-- DASH ESPECIFICO DE ESTABELECIMENTOS E ESPECIALIDADES  -->
            <div id="accordion2">
                <div class="acord2">
                    <div class="" id="headingTwo">
                      <div class="mb-0">
                        <button class="btn bg-white dropdown-toggle float-right" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                        </button>
                        <h4>
                           Estabelecimentos
                        </h4>

                        <li style="border-top: 2px #efefef solid; margin-top: 0px; margin-bottom: 0px; display: block;"> </li>
                                <br>
                        
                        
                      </div>
                    </div>



                <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordion2">
                      <div class="">
                        <div class="row col-lg-12">
                                <div class="col-md-3">
                                  <div class="card bg-success" style="box-shadow: 3px 3px 3px;" >
                                     <div class="card-header text-white bg-success">

                                       <i class="fas fa-hospital"></i> Estabelecimentos Ativos

                                     </div>
                                            <div class="card-body bg-white">

                                                <div class="row col-lg-12">
                                                <h1 class="card-title text-success">{{count($estabA)}}</h1>
                                                 <strong class="text-success" style="padding: 15px 6px;"> Qtde</strong>
                                                <p class="card-text" style="text-align: center">Estabelecimentos Ativos.</p>
                                                </div>   
                                            </div>
                                  </div>
                                  <br>
                                </div>


                                 <div class="col-md-3">
                                  <div class="card bg-danger" style="box-shadow: 3px 3px 3px;">
                                     <div class="card-header text-white bg-danger">

                                        <i class="fas fa-hospital"></i> Estabelecimentos Inativos

                                     </div>
                                            <div class="card-body bg-white">

                                                <div class="row col-lg-12">
                                                <h1 class="card-title text-danger">{{count($estabI)}}</h1>
                                                 <strong class="text-danger" style="padding: 15px 6px;"> Qtde</strong>
                                                <p class="card-text col-lg-12" style="text-align: center">Estabelecimentos Inativos.</p>
                                                </div>   
                                            </div>
                                  </div>
                                  <br>
                                 </div>


                                 <div class="col-md-3">
                                  <div class="card bg-info" style="box-shadow: 3px 3px 3px;">
                                     <div class="card-header text-white bg-info">

                                       <i class="fas fa-user-md"></i> Especialidades

                                     </div>
                                            <div class="card-body bg-white">

                                                <div class="row col-lg-12">
                                                <h1 class="card-title text-info">{{count($espec)}}</h1>
                                                 <strong class="text-info" style="padding: 15px 6px;"> Qtde</strong>
                                                <p class="card-text col-12" style="text-align: center">Especialidades Cadastradas.</p>
                                                </div>   
                                            </div>
                                  </div>
                                  <br>
                              </div>
                        </div> 
                    </div>
                </div>
              </div>
            </div>
            <!-- FIM DASH ESTABELECIMENTOS E ESPECIALIDADES -->
            <br>

            <!-- DASH DAS FROTAS NO SISTEMA -->
            <div id="accordion3">
                <div class="acord3">
                    <div class="frota" id="headingThree">
                      <div class="mb-0">
                        <button class="btn bg-white dropdown-toggle float-right" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                        </button>
                        <h4>
                           Frotas
                        </h4>

                        <li style="border-top: 2px #efefef solid; margin-top: 0px; margin-bottom: 0px; display: block;"> </li>
                                <br>
                        
                        
                      </div>
                    </div>



                <div id="collapseThree" class="collapse show" aria-labelledby="headingThree" data-parent="#accordion3">
                      <div class="">
                        <div class="row col-lg-12">
                                <div class="col-md-3">
                                  <div class="card bg-success" style="box-shadow: 3px 3px 3px;">
                                     <div class="card-header text-white bg-success">

                                        <i class="fas fa-ambulance"></i> Frotas Ativas

                                     </div>
                                            <div class="card-body bg-white">

                                                <div class="row col-lg-12">
                                                <h1 class="card-title text-success">{{count($frotaA)}}</h1>
                                                 <strong class="text-success" style="padding: 15px 6px;"> Qtde</strong>
                                                <p class="card-text col-lg-12" style="text-align: center">Frotas Ativas.</p>
                                                </div>   
                                            </div>
                                  </div>
                                  <br>
                                </div>


                                 <div class="col-md-3">
                                  <div class="card bg-danger" style="box-shadow: 3px 3px 3px;">
                                     <div class="card-header text-white bg-danger">

                                        <i class="fas fa-ambulance"></i> Frotas Inativas

                                     </div>
                                            <div class="card-body bg-light">

                                                <div class="row col-lg-12">
                                                <h1 class="card-title text-danger">{{count($frotaI)}}</h1>
                                                 <strong class="text-danger" style="padding: 15px 6px;"> Qtde</strong>
                                                <p class="card-text col-lg-12" style="text-align: center">Frotas Inativas.</p>
                                                </div>   
                                            </div>
                                  </div>
                                  <br>
                                 </div>


                                 <div class="col-md-3">
                                  <div class="card bg-info" style="box-shadow: 3px 3px 3px;">
                                     <div class="card-header text-white bg-info">

                                       <i class="fas fa-ambulance"></i> Tipos de Frota

                                     </div>
                                            <div class="card-body bg-light">

                                                <div class="row col-lg-12">
                                                <h1 class="card-title text-info">{{count($tipos)}}</h1>
                                                 <strong class="text-info" style="padding: 15px 6px;"> Qtde</strong>
                                                <p class="card-text col-12" style="text-align: center">Tipos de Frotas.</p>
                                                </div>   
                                            </div>
                                  </div>

                                </div>
                        </div> 
                    </div>
                </div>
              </div>
            </div>
            <!--  FIM DO DASH DAS FROTAS-->
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

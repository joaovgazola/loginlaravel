<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::any('/', function () {
    return view('welcome');
});

Auth::routes();

Route::any('/home', 'HomeController@index')->name('home');
//Rotas do gestor ..

//Rota Usurario
Route::any('/meusdados', 'UsuarioController@index')->name('minhaconta');
Route::any('/novasenha', 'UsuarioController@senha')->name('novasenha');
Route::any('/senha/salvar', 'UsuarioController@savesenha')->name('salvarsenha');

//Rota Funcionario
Route::any('/funcionario', 'FuncController@index')->name('func');
Route::any('/funcionario/busca', 'FuncController@show')->name('funcbusca');
Route::any('/funcionario/status/{id}', 'FuncController@trocaStatus')->name('funcstatus');
Route::any('/funcionario/novo', 'FuncController@create');
Route::any('/funcionario/save', 'FuncController@save')->name('funcsalvar');
Route::any('/funcionario/{teste}/edit', 'FuncController@edit');
Route::any('/funcionario/{idFunc}/update', 'FuncController@update')->name('funcupdate');
Route::any('/usuario', 'FuncController@createUser');
// Fim Rota Funcionario

// Rota Estabelecimento
Route::any('/estabelecimento', 'EstabelecimentoController@index')->name('estabelecimento');
Route::any('/estabelecimento/busca', 'EstabelecimentoController@show')->name('estabelecimentobusca');
Route::any('/estabelecimento/status/{id}', 'EstabelecimentoController@trocaStatus')->name('estabstatus');
Route::any('/estabelecimento/novo', 'EstabelecimentoController@create');
Route::any('/estabelecimento/save', 'EstabelecimentoController@save')->name('estabsalvar');
Route::any('/estabelecimento/{id}/editar', 'EstabelecimentoController@edit')->name('estabEdit');
Route::any('/estabelecimento/{id}/update', 'EstabelecimentoController@update')->name('estabUpdate');

// Fim Rota Estabelecimento

//Rota Frota
Route::any('/frota', 'FrotaController@index')->name('frota');
Route::any('/frota/busca', 'FrotaController@show')->name('frotabusca');
Route::any('/frota/novo', 'FrotaController@create')->name('novafrota');
Route::any('/frota/save', 'FrotaController@savefrota')->name('frotasalvar');
Route::any('/frota/status/{id}', 'FrotaController@trocaStatus')->name('frotastatus');
Route::any('/frota/{id}/edit', 'FrotaController@edit')->name('frotaedit');
Route::any('/frota/{id}/update', 'FrotaController@update')->name('frotaupdate');

//Fim Rota Frota

// rota especialidade
Route::any('/especialidade', 'EspecialidadeController@index')->name('especialidade');
Route::any('/especialidade/novo', 'EspecialidadeController@index')->name('novaespec');
Route::any('/especialidade/saveall', 'EspecialidadeController@saveall')->name('esptoestab');
Route::any('/especialidade/save', 'EspecialidadeController@save')->name('especsalvar');
Route::any('/especialidade/{id}/edit', 'EspecialidadeController@editespec')->name('editespec');
Route::any('/especialidade/{id}/update', 'EspecialidadeController@updatespec')->name('updatespec');
Route::any('/especialidade/editname', 'EspecialidadeController@editName')->name('editname');

//rota tipo
Route::any('/tipo/save', 'TipoController@save')->name('tiposalvar');
Route::any('/tipo/busca', 'TipoController@busca')->name('buscatipo');
Route::any('/tipo/update', 'TipoController@update')->name('tipoupdate');


//TARM
Route::any('/TARM', 'TarmController@index')->name('tarm');
Route::any('/TARM/save', 'TarmController@save')->name('chamadoSalvar');
<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFuncsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('funcs', function (Blueprint $table) {
           $table->bigIncrements('id');
            $table->string('nome');
            $table->integer('matricula')->unique();
            $table->string('sexo');
            $table->string('rg');
            $table->string('cpf')->unique();
            $table->date('nascimento');
            $table->string('contato')->nullable();
            $table->string('contato2')->nullable();
            $table->string('perfil');
            $table->string('email')->unique();
            $table->boolean('status')->default(true);
            $table->unsignedBigInteger('idLocal')->unique();
            $table->foreign('idLocal')->references('id')->on('locais')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('funcs');
    }
}

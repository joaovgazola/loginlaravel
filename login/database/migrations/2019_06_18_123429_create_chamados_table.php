<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChamadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chamados', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('protocolo');
            $table->string('contato');
            $table->string('nomeSolicitante')->nullable();
            $table->string('tipoChamado');
            $table->longText('fato');
            $table->string('apelido');
            $table->string('prioridade');
            $table->unsignedBigInteger('medico');
            $table->string('observacoes')->nullable();
            $table->unsignedBigInteger('idLocal')->unique();
            $table->foreign('idLocal')->references('id')->on('locais')->onDelete('cascade');
            $table->foreign('medico')->references('id')->on('funcs');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chamados');
    }
}

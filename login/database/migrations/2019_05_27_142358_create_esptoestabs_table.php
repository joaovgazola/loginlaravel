<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEsptoestabsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('esptoestabs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('idEstab');
            $table->foreign('idEstab')->references('id')->on('estabelecimentos')->onDelete('cascade');
            $table->unsignedBigInteger('idEsp');
            $table->foreign('idEsp')->references('id')->on('especialidades')->onDelete('cascade');
            $table->unique(['idEstab', 'idEsp']);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('esptoestabs');
    }
}

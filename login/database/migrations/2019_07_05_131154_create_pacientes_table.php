<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePacientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pacientes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table ->string('paciente')->nullable();
            $table ->integer('idade')->nullable();
            $table->string('faixaEtaria');
            $table->string('sexo')->nullable();
            $table->string('incidentePaciente')->nullable();
            $table->unsignedBigInteger('idChamado')->unique();
            $table->foreign('idChamado')->references('id')->on('chamados')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pacientes');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Frota extends Model
{
    protected $fillable = [
        'placa', 'tipo', 'situacao', 'numero', 'quilometragem'
    ];
}

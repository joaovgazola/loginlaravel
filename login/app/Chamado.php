<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chamado extends Model
{
    protected $fillable = [
        'protocolo', 
        'contato', 
        'nomeSolicitante', 
        'tipoChamado', 
        'fato',
        'apelido', 
        // 'idLocal', 
        'prioridade', 
        'medico', 
        'observacoes'
    ];
}

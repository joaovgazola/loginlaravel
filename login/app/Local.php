<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Local extends Model
{
    protected $table = 'locais';
    
    protected $fillable = [
        'cep',
        'logradouro',
        'numero',
        'bairro',
        'referencia',
        'bairro',
        'cidade',
        'uf',

    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Esptoestab extends Model
{
    protected $fillable = [
      'idEstab', 'idEsp'
    ];
}

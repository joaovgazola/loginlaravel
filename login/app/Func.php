<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Func extends Model
{
    protected $fillable =[
        'nome',
        'matricula',
        'cpf',
        'perfil',
        'sexo',
        'rg',
        'contato',
        'contato2',
        'nascimento',
        'idLocal',
        'status',
        'email'
    ];



}

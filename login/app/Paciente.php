<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paciente extends Model
{
    protected $fillable = [
        'paciente',
        'idade',
        'faixaEtaria',
        'sexo',
        'incidentePaciente',
        'idChamado'
    ];

}

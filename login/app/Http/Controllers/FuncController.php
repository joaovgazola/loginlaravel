<?php

namespace App\Http\Controllers;

use App\Func;
use App\Local;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Mockery\Exception;
use phpDocumentor\Reflection\Types\Nullable;
use phpDocumentor\Reflection\Types\String_;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Mail;
use App\Mail\NovoUsuario;


class FuncController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function validaCPF($cpf) {

        // Verifica se um número foi informado
        if(empty($cpf)) {
            return false;
        }
    
        // Elimina possivel mascara
        $cpf = preg_replace("/[^0-9]/", "", $cpf);
        $cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);
        
        // Verifica se o numero de digitos informados é igual a 11 
        if (strlen($cpf) != 11) {
            return false;
        }
        // Verifica se nenhuma das sequências invalidas abaixo 
        // foi digitada. Caso afirmativo, retorna falso
        else if ($cpf == '00000000000' || 
            $cpf == '11111111111' || 
            $cpf == '22222222222' || 
            $cpf == '33333333333' || 
            $cpf == '44444444444' || 
            $cpf == '55555555555' || 
            $cpf == '66666666666' || 
            $cpf == '77777777777' || 
            $cpf == '88888888888' || 
            $cpf == '99999999999') {
            return false;
         // Calcula os digitos verificadores para verificar se o
         // CPF é válido
         } else {   
            
            for ($t = 9; $t < 11; $t++) {
                
                for ($d = 0, $c = 0; $c < $t; $c++) {
                    $d += $cpf{$c} * (($t + 1) - $c);
                }
                $d = ((10 * $d) % 11) % 10;
                if ($cpf{$c} != $d) {
                    return false;
                }
            }
    
            return true;
        }
    }



    function validar($valida, $id){
        // checa se ja existe esse cpf no BD
        $checkcpf =  Func::query();
        //se não for uma checagem de edição
        if($id == 0){
            $checkcpf = $checkcpf->where('cpf', $valida->cpf)->first();
            if (FuncController::validaCPF($valida->cpf)){
                if (!empty($checkcpf)) {
                    Session::flash('erro-cadastro', 'CPF já cadastrado no sistema');
                    return false;

                }
            }
            else{
                Session::flash('erro-cadastro', 'CPF inválido');
                return false;
            }
        }
        //se for uma checagem de edição
        else{
            $checkcpf = $checkcpf->where('cpf', $valida->cpf)->get();
            foreach ($checkcpf as $check){
                if($check->id != $id){
                    Session::flash('erro-cadastro', 'CPF já cadastrado no sistema');
                    return false;
                }
            }
            if (!FuncController::validaCPF($valida->cpf)){
                Session::flash('erro-cadastro', 'CPF inválido');
                return false;
            }
        }
        //checa se existe outra matricula no DB
        $checkmatricula = Func::query();
        //se não for uma checagem de edição
        if($id == 0){
            $checkmatricula = $checkmatricula->where('matricula', $valida->matricula)->first();
            if (!empty($checkmatricula)) {
                Session::flash('erro-cadastro', 'Matrícula já cadastrado no sistema');
                return false;
            }
        }
        //se for uma checagem de edição
        else{
            $checkmatricula = $checkmatricula->where('matricula', $valida->matricula)->get();
            foreach ($checkmatricula as $check){
                if($check->id != $id){
                    Session::flash('erro-cadastro', 'Matrícula já cadastrado no sistema');
                    return false;
                }
            }
        }

        //checa data de nescimento
        $ano = idate('Y')-16;
        $data = $ano.'/'.idate('m').'/'.idate('d');
        if(strtotime($valida->nascimento) > strtotime($data)){
            Session::flash('erro-cadastro', 'Data de nascimento inválida');
            return false;
        }

        //checa se existe outro email no DB
        $checkemail = Func::query();
        //se não for uma checagem de edição
        if($id == 0){
            $checkemail = $checkemail->where('email', $valida->email)->first();
            if (!empty($checkemail)) {
                Session::flash('erro-cadastro', 'E-mail já cadastrado no sistema');
                return false;
            }
        }
        //se for uma checagem de edição
        else{
            $checkemail = $checkemail->where('email', $valida->email)->get();
            foreach ($checkemail as $check){
                if($check->id != $id){
                    Session::flash('erro-cadastro', 'E-mail já cadastrado no sistema');
                    return false;
                }
            }
        }

        return true;
    }

    public function index()
    {
        $func = Func::query();
        $func = $func->orderBy('nome')->get();
        return view('gestor.funcionario', ['func' => $func]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('gestor.cadastro-func');
    }

    /**
     * Valida os dados recebidos do formulario.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    function save(Request $request){
        if (FuncController::validar($request, 0)){

            $local=new Local();
            $local->cep = $request->cep;
            $local->logradouro = $request->logradouro;
            $local->numero = $request->numero;
            $local->referencia = $request->referencia;
            $local->bairro = $request->bairro;
            $local->cidade= $request->cidade;
            $local->uf = $request->uf;
            $local->save();


            $func = new Func();
            $func ->nome = $request->nome;
            $func->matricula = $request->matricula;
            $func->cpf = $request->cpf;
            $func->perfil = $request->perfil;
            $func->contato = $request->contato;
            $func->contato2 = $request->contato2;
            $func->email = $request->email;

            $func->sexo = $request->sexo;
            $func->rg = $request->rg;
            $func->nascimento = $request->nascimento;
            $func->idLocal = $local->id;
            $func->save();

            $this->createUser($func);
            return redirect()->to(route('func'));
        }
        else{
            return view('gestor.cadastro-func', ['func'=> $request]);
        }
    }

    /**
     * Mostra  os dad*
     * @param  \App\Func  $funcs
     * @return \Illuminate\Http\Response
     */

    public function show(Request $request)
    {
        $func = Func::query();

        if ($request->nome != null) {
            $func->where('nome', 'like', '%'.$request->nome.'%');
        }
        if ($request->cpf != null) {
            $func->where('cpf', $request->cpf);
        }
        if ($request->matricula != null) {
            $func->where('matricula', $request->matricula);
        }
        if ($request->perfil != null) {
            $func->where('perfil', $request->perfil);
        }
        if ($request->status != null) {
            if($request->status == 'true') {
                $func->where('status', 1);
            }
            else{
                $func->where('status', 0);
            }
        }
        $func = $func->orderBy('nome')->get();
        return view('gestor.funcionario', ['func'=>$func]);
    }

    /**
     *
     * Autentica usuario
     * @param  Auth
     * @param  \App\Func
     * @return
     */


    public static function authName(){
        try {
            $cpf = Auth::user()->username;
            $func = Func::where('cpf', $cpf)->first();
            return $func->nome;
        }   catch (Exception $exception){
            return "não cadastrado";
        }
    }

    /**
     *
     * puxa os dados para ser editado
     * @param  \App\Func
     * @param  \App\Local
     *  @param  \App\User
     * @return \Illuminate\Http\Response
     */

    public function edit($teste)
    {
        $funcionarios = Func::query();
        $func = $funcionarios->where('id', $teste)->first();

        $locals = Local::query();
        $local = $locals->where('id',$func->idLocal)->first();

        if (empty($func)) {
            Session::flash('falha-funcionario', 'Funcionário inválido');
            return redirect()->to(route('func'));
        } else{
            return view('gestor.Edit-func', ['func'=>$func, 'local'=>$local,'idFunc' => $teste, 'email' => $func->email]);
        }
    }

    /**
     *Edirt os dados do cadastro de funcionario.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Func  $idFunc
     * @param  \App\Local

     * @return
     */
    public function update(Request $request, $idFunc)
    {
        if (FuncController::validar($request, $idFunc)) {
            $func = Func::findOrFail($idFunc);
            $local = Local::findOrFail($func->idLocal);
            $check = false;
            $check2 = false;
            if ($func->cpf == Auth::user()->username) {
                $check2 = true;
            }
            $local->cep = $request->cep;
            $local->logradouro = $request->logradouro;
            $local->numero = $request->numero;
            $local->referencia = $request->referencia;
            $local->bairro = $request->bairro;
            $local->cidade = $request->cidade;
            $local->uf = $request->uf;
            $local->update();

            $func->nome = $request->nome;
            if ($func->cpf != $request->cpf) {
                $aux = Func::query();
                $aux = $aux->where('cpf', $request->cpf)->first();
                if (empty($aux)) {
                    if ($check2) {
                        Session::flash('alter_cpf', 'CPF do usuário que estava logado foi alterado');
                    }
                    $func->cpf = $request->cpf;
                    $check = true;

                }
            }
            $func->rg = $request->rg;
            $func->matricula = $request->matricula;
            $func->nascimento = $request->nascimento;
            $func->sexo = $request->sexo;
            $func->contato = $request->contato;
            $func->contato2 = $request->contato2;
            $aux = $func->perfil;
            if ($func->perfil != $request->perfil) {
                $func->perfil = $request->perfil;
                if ($check2) {
                    Session::flash('alter_perfil', 'Perfil do usuário que estava logado foi alterado');
                }
                $check = true;
            }

            if ($func->email != $request->email) {
                $func->email = $request->email;
                $check = true;
                if ($aux == 'Tarm' or $aux == 'Médico Regulador' or $aux == 'Rádio Operador' or $aux == 'Gestor') {
                    if ($request->perfil == 'Tarm' or $request->perfil == 'Médico Regulador' or $request->perfil == 'Rádio Operador' or $request->perfil == 'Gestor') {
                        if ($check2) {
                            Session::flash('alter_email', 'Email do usuário que estava logado foi alterado, sua nova senha está no novo email');
                        }
                        $senha = $this->randSenha(8);
                        $auxiliar = User::query();
                        $auxiliar = $auxiliar->where('id_func', $func->id)->first();
                        $auxiliar->password = Hash::make($senha);
                        $auxiliar->save();
                        Mail::to($func->email)->send(new NovoUsuario('Seu usuário no Sin-Samu foi atualizado!', $senha));
                    } else {
                        Session::flash('alter_email', 'Email do usuário que estava logado foi alterado');
                    }
                }
            }
            $func->update();
            Session::flash('edit-funcionario', 'Funcionário editado com sucesso!');
            if ($aux != $request->perfil) {
                if ($aux == 'Tarm' or $aux == 'Médico Regulador' or $aux == 'Rádio Operador' or $aux == 'Gestor') {
                    $user = User::query();
                    $user = $user->where('id_func', $func->id)->first();
                    if ($request->perfil == 'Enfermeiro' or $request->perfil == 'Motorista') {
                        $user->delete();
                    } else {
                        $user->perfil = $request->perfil;
                        $user->email = $request->email;
                        $user->username = $request->cpf;
                        $user->update();
                    }
                } else {
                    if ($request->perfil == 'Tarm' or $request->perfil == 'Médico Regulador' or $request->perfil == 'Rádio Operador' or $request->perfil == 'Gestor') {
                        $this->createUser($func);
                    }
                }
            } else {
                if ($aux == 'Tarm' or $aux == 'Médico Regulador' or $aux == 'Rádio Operador' or $aux == 'Gestor') {
                    $user = User::query();
                    $user = $user->where('id_func', $func->id)->first();
                    $user->email = $request->email;
                    $user->username = $request->cpf;
                    $user->update();
                }
            }
            if ($check and $check2) {
                Auth::logout();
                return redirect()->to(route('login'));
            }
            return redirect()->to(route('func'));


        }
        else{
            return redirect()->back();
        }
    }
    /**
     * Função que troca status do funcionario
     * @param $id
     * @return \App\Func
     */

    public function trocaStatus($id){
        $func = Func::findOrFail($id);
        if($func->status == true){
            $func->update(['status'=>false]);
            Session::flash('falha-funcionario', $func->nome." : Desativado");
        }
        else{
            $func->update(['status'=>true]);
            Session::flash('sucesso-funcionario', $func->nome." : Ativado");
        }
        return redirect()->back();
    }

    /**
     * Função que cria senha aleatória
     * @param   $n
     * @return string
     */

    function randSenha($n) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';

        for ($i = 0; $i < $n; $i++) {
            $index = rand(0, strlen($characters) - 1);
            $randomString .= $characters[$index];
        }

        return $randomString;
    }


    /**
     * Função que cria usuário a partir do cadastro de funcionario e manda email com a senha
     *
     * @param  \App\Func  $func
     * @return \App\User
     */

    protected function createUser($func)
    {
        $senha =$this->randSenha(8);

        if ($func->perfil == 'Tarm'){
            Session::flash('sucesso-funcionario', 'Usuário do tipo TARM criado ');
            Mail::to($func->email)->send(new NovoUsuario('Seu usuário no Sin-Samu foi criado !',$senha));
            return User::create([
                'id_func' => $func->id,
                'username' => $func->cpf,
                'perfil' => 'Tarm',
                'email' => $func->email,
                'password' => Hash::make($senha),
            ]);
        }

        if ($func->perfil  == 'Médico Regulador'){
            Session::flash('sucesso-funcionario', 'Usuário do tipo Médico Regulador criado ');
            Mail::to($func->email)->send(new NovoUsuario('Seu usuário no Sin-Samu foi criado !',$senha));

            return User::create([
                'id_func' => $func->id,
                'username' => $func->cpf,
                'perfil' => 'Médico Regulador',
                'email' => $func->email,
                'password' =>Hash::make($senha),
            ]);
        }
        if ($func->perfil == 'Rádio Operador'){
            Session::flash('sucesso-funcionario', 'Usuário do tipo Rádio Operador criado ');
            Mail::to($func->email)->send(new NovoUsuario('Seu usuário no Sin-Samu foi criado!',$senha));

            return User::create([
                'id_func' => $func->id,
                'username' => $func->cpf,
                'perfil' => 'Rádio Operador',
                'email' => $func->email,
                'password' => Hash::make($senha),
            ]);
        }

        if ($func->perfil == 'Gestor'){
            Session::flash('sucesso-funcionario', 'Usuário do tipo Gestor criado ');
            Mail::to($func->email)->send(new NovoUsuario('Seu usuário no Sin-Samu foi criado!',$senha));
            return User::create([
                'id_func' => $func->id,
                'username' => $func->cpf,
                'perfil' => 'Gestor',
                'email' => $func->email,
                'password' => Hash::make($senha),

            ]);
        }

    }
    

}

<?php

namespace App\Http\Controllers;

use App\Func;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;

class UsuarioController extends Controller
{
    public function index(){
        $func = Func::query();
        $func = $func->where('cpf', Auth::user()->username)->first();
        return view('gestor.Usuario.index', ['func'=>$func]);
    }

    public function senha(){
        $usename = Auth::user()->username;
        return view('gestor.Usuario.novasenha', ['username'=>$usename]);
    }

    public function savesenha(Request $request){
        if (!(Hash::check($request->senhaAtual, Auth::user()->getAuthPassword()))) {
            // The passwords matches
            Session::flash('erro', 'A senha informada está incorreta');
            return redirect()->back();
        }
        if(strcmp($request->senhaAtual, $request->novaSenha) == 0){
            //Current password and new password are same
            Session::flash('erro', 'Senha atual e senha nova não podem ser identicas');
            return redirect()->back();
        }
        if (strcmp($request->novaSenha, $request->confirm) != 0){
            Session::flash('erro', 'Nova senha não bate com a confirmação');
            return redirect()->back();
        }
        //Change Password
        $user = Auth::user();
        $user->password = Hash::make($request->novaSenha);
        $user->save();
        Auth::logout();
        return redirect()->route('home');
    }
}

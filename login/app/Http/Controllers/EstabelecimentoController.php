<?php

namespace App\Http\Controllers;

use App\Especialidade;
use App\Esptoestab;
use App\Local;
use App\Estabelecimento;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class EstabelecimentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function teste(){
        
    return view ('gestor.especialidade');

    }
    public function index()
    {

        $estab = Estabelecimento::query();
        $estab = $estab->orderBy('nome')->get();
        $esp = Especialidade::query();
        $esp = $esp->orderBy('nome')->get();
        return view('gestor.estabelecimento',  ['estab' => $estab, 'especs'=>$esp]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('gestor.cadastro-estabelecimento');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $local = new Local();
        $local->cep = $request->cep;
        $local->logradouro = $request->logradouro;
        $local->numero = $request->numero;
        $local->referencia = $request->referencia;
        $local->bairro = $request->bairro;
        $local->cidade = $request->cidade;
        $local->uf = $request->uf;
        $local->save();

        $estab = new Estabelecimento();
        $estab->nome = $request->nome;
        $estab->idLocal = $local->id;

        $estab->save();
        return redirect()->route('novaespec', ['id'=>$estab->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Estabelecimento  $estabelecimento
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $estab = Estabelecimento::query();

        if ($request->nome != null) {
            $estab->where('nome', 'like', '%'.$request->nome.'%');
        }

        if ($request->especialidade != null) {
            $esptoestabs = Esptoestab::query();
            $esptoestabs = $esptoestabs->where('idEsp', $request->especialidade)->get();
            $ids = array();
            foreach ($esptoestabs as $esptoestab){
                array_push($ids, $esptoestab->idEstab);
            }
            $estab->whereIn('id', $ids);
        }

        if ($request->situacao != null) {
            if($request->situacao == '1') {
                $estab->where('status', true);
            }
            else{
                $estab->where('status', false);
            }
        }

        $estab = $estab->orderBy('nome')->get();
        $esp = Especialidade::query();
        $esp = $esp->orderBy('nome')->get();
        return view('gestor.estabelecimento', ['estab'=>$estab, 'especs'=>$esp]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Estabelecimento  $estabelecimento
     * @return \Illuminate\Http\Response
     */
    public function edit(Estabelecimento $estabelecimento, $id)
    {
        $estabelecimentos = Estabelecimento::query();
        $estab = $estabelecimentos->where('id', $id)->first();

        if (empty($estab)) {
            Session::flash('falha-estabelecimento', 'Estabelecimento invalido');
            return redirect()->to(route('estabelecimento'));
        } else{

            $locals = Local::query();
            $local = $locals->where('id', $estab->idLocal)->first();

            return view('gestor.Edit-estabelecimento', ['local' => $local, 'estab' => $estab]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Estabelecimento  $estabelecimento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id )
    {
        $estab = Estabelecimento::findOrFail($id);
        $local = Local::findOrFail($estab->idLocal);
        
        $local->cep = $request->cep;
        $local->logradouro = $request->logradouro;
        $local->numero = $request->numero;
        $local->referencia = $request->referencia;
        $local->bairro = $request->bairro;
        $local->cidade = $request->cidade;
        $local->uf = $request->uf;
        $local->update();

        $estab->nome = $request->nome;
        $estab->idLocal = $local->id;
        $estab->update();

        Session::flash('edit-estab', 'Estabelecimento editado com sucesso!');
        return redirect()->route('editespec',['id' => $estab->id]);
    }

    public function trocaStatus($id){
        $estab = Estabelecimento::findOrFail($id);
        if($estab->status == true){
            $estab->update(['status'=>false]);
            Session::flash('falha-estabelecimento', $estab->nome." : Desativado");
        }
        else{
            $estab->update(['status'=>true]);
            Session::flash('sucesso-estabelecimento', $estab->nome." : Ativado");
        }
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Estabelecimento  $estabelecimento
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}

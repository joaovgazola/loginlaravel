<?php

namespace App\Http\Controllers;

use App\Frota;
use App\Tipo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class FrotaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $frota = Frota::query();
        $frota = $frota->orderBy('numero')->get();
        $tipos = Tipo::query();
        $tipos = $tipos->orderBy('descricao')->get();
        return view('gestor.frota', ['frota'=>$frota, 'tipos'=>$tipos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tipos = Tipo::query();
        $tipos = $tipos->orderBy('descricao')->get();
        return view('gestor.cadastro-frota', ['tipos'=>$tipos]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function savefrota(Request $request)
    {
        $check1 = Frota::query();
        $check1 = $check1->where('placa', $request->placa)->first();
        if (!empty($check1)) {
            Session::flash('erro', 'Placa já cadastrada');
            return redirect()->back();
        }
        $check2 = Frota::query();
        $check2 = $check2->where('numero', $request->numero)->first();
        if (!empty($check2)) {
            Session::flash('erro', 'Número já cadastrado');
            return redirect()->back();
        }
        $frota = new Frota();
        $frota->numero = $request->numero;
        $frota->placa = $request->placa;
        $frota->quilometragem = $request->quilometragem;
        $frota->tipo_id = $request->tipo;
        $frota->save();
        Session::flash('sucesso-frota', 'Frota cadastrada com sucesso!');
        return redirect()->to(route('frota'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Frota  $frota
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $frota = Frota::query();

        if ($request->numero != null) {
            $frota->where('numero', $request->numero);
        }
        if ($request->placa != null) {
            $frota->where('placa', 'like', '%'.$request->placa.'%');
        }
        if ($request->tipo != null) {
            $frota->where('tipo_id', $request->tipo);
        }
        if ($request->situacao != null) {
            if($request->situacao == '1') {
                $frota->where('situacao', true);
            }
            else{
                $frota->where('situacao', false);
            }
        }
        $frota = $frota->orderBy('numero')->get();
        $tipos = Tipo::query();
        $tipos = $tipos->orderBy('descricao')->get();
        return view('gestor.frota', ['frota'=>$frota, 'tipos'=>$tipos]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Frota  $frota
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $frotas =  Frota::query();
        $frota = $frotas->where('id', $id)->first();

        $tipos = Tipo::query();
        $tipos = $tipos->orderBy('descricao')->get();
        
        $teste = $tipos->where('id', $frota->tipo_id)->first();


        if (empty($frota)){
            Session::flash('falha-frota', 'Frota invalida!');
            return redirect()->to(route('frota'));
        }

        return view('gestor.Edit-frota', ['tipos'=>$tipos, 'frota'=>$frota, 'teste'=>$teste]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Frota  $frota
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $frotas =  Frota::query();
        $frota = $frotas->where('id', $id)->first();
        $numero = Frota::query();
        $numero = $numero->where('numero', $request->numero)->get();
        foreach($numero as $check){
            if($check->id != $id and $check->numero == $request->numero)
            {
                Session::flash('erro', 'Frota com mesmo numero já cadastrada');
                return redirect()->to(route('frotaedit', ['id'=>$id]));
            }
        }
        $frota->numero = $request->numero;
        $placa = Frota::query();
        $placa = $placa->where('placa', $request->placa)->get();
        foreach($placa as $check){
            if($check->id != $id and $check->placa == $request->placa)
            {
                Session::flash('erro', 'Frota com mesma placa já cadastrada');
                return redirect()->to(route('frotaedit', ['id'=>$id]));

            }
        }
        $frota->placa = $request->placa;
        $frota->quilometragem = $request->quilometragem;
        $frota->tipo_id = $request->tipo;
        $frota->update();
        Session::flash('sucesso-frota', 'Frota editada com sucesso!');
        return redirect()->to(route('frota'));
    }

    public function trocaStatus($id){
        $frota = Frota::findOrFail($id);
        if($frota->situacao == true){
            $frota->update(['situacao'=>false]);
            Session::flash('falha-frota', $frota->placa." : Desativado");
        }
        else{
            $frota->update(['situacao'=>true]);
            Session::flash('sucesso-frota', $frota->placa." : Ativado");
        }
        return redirect()->back();
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Frota  $frota
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $frota = Frota::findOrFail($id);
        $frota->delete();
        return redirect()->back();
    }
}

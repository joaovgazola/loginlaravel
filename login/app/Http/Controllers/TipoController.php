<?php

namespace App\Http\Controllers;
use App\Tipo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class TipoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tipo = Tipo::query();
        $tipo = $tipo->orderBy('descricao')->get();
        return view('gestor.tipo', ['tipo'=>$tipo]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('gestor.cadastro-tipo');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $check = Tipo::query();
        $check = $check->where('descricao', '=', $request->descricao)->first();
        if(empty($check)){
            $tipo = new Tipo($request->all());
            $tipo->save();
        }
        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function busca()
    {
        $tipos = Tipo::query();
        $tipos = $tipos->orderBy('descricao')->get();
        return $tipos;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $tipo = Tipo::findOrFail($request->editid);

        $tipo->descricao = $request->editdescricao;
        $tipo->update();
        Session::flash('sucesso-frota', 'Tipo da frota editado com sucesso!');
        return redirect()->back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

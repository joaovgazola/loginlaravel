<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Chamado;
use App\Local;
use App\Func;
use App\Paciente;

class TarmController extends Controller
{
    public function index(){

        $funcs = Func::query();
        $Medicos = $funcs->where('perfil','Médico Regulador');
        $Medicos = $Medicos->orderBy('nome')->get();
        return view('tarm.atendimento',['Medicos' => $Medicos]);
    }

    public function save(Request $request){
   
        $local=new Local();
        $local->cep = $request->cep;
        $local->logradouro = $request->logradouro;
        $local->numero = $request->numero;
        $local->referencia = $request->referencia;
        $local->bairro = $request->bairro;
        $local->cidade= $request->cidade;
        $local->uf = $request->uf;
        $local->save();

        $chamado = new Chamado();
        $chamado->protocolo = $request->protocolo;
        $chamado->contato = $request->contato;
        $chamado->nomeSolicitante = $request->nomeSolicitante;
        $chamado->prioridade = $request->prioridade;
        $chamado->tipoChamado = $request->tipoChamado;
        $chamado->fato = $request->fato;
        $chamado->apelido = $request->apelido;
        $chamado->medico = $request->medico;
        $chamado->observacoes = $request->observacoes;
        $chamado->idLocal = $local->id;
        $chamado->save();
             
        $paciente =  new Paciente();
        $paciente->paciente = $request->paciente;
        $paciente->idade = $request->idade;
        $paciente->faixaEtaria = $request->faixaEtaria;
        $paciente->incidentePaciente = $request->incidentePaciente;
        $paciente->sexo = $request->sexo;
        $paciente->idChamado= $chamado->id;
        $paciente->save(); 


   
       
        
    
        return redirect()->to(route('tarm'));
        
    }
}

<?php

namespace App\Http\Controllers;

use App\Especialidade;
use App\Esptoestab;
use App\Estabelecimento;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class EspecialidadeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $espec = Especialidade::query();
        $espec = $espec->orderBy('nome')->get();
        return view('gestor.especialidade', ['espec'=>$espec]);
    }
    public function editespec($id){

        $espec = Especialidade::query();
        $espec = $espec->orderBy('nome')->get();
        
        $espectoestab = Esptoestab::query();
        $atives = $espectoestab->where('idEstab', $id);
        $atives = $atives->orderBy('id')->get();
       
        foreach($espec as $especialidade){
            $especialidade->status = false;
            foreach($atives as $ative){
                if($especialidade->id == $ative->idEsp){
                    $especialidade->status = true;
                }
            }
        }

        return view('gestor.Edit-especialidade', ['espec'=>$espec, 'id' => $id]);
    }
    public function updatespec(Request $request, $id){
        $Esptoestab = Esptoestab::query();
        $Esptoestab = $Esptoestab->where('idEstab', $id);
        $Esptoestab->delete();

        $idEstab = $request->id;
        $idsesp = $request->get('esp');
        if(!empty($idsesp))
        {
            foreach ($idsesp as $idEsp) {
                $esptoestab = new Esptoestab();
                $esptoestab->idEsp = $idEsp;
                $esptoestab->idEstab = $idEstab;
                $esptoestab->save();
            }
        }
        Session::flash('edit-estab', 'Estabelecimento editado com sucesso!');
        return redirect()->route('estabelecimento');
    }

    /**
     * Save a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $check = Especialidade::query();
        $check = $check->where('nome', $request->nome)->first();
        if(empty($check)) {
            $esp = new Especialidade($request->all());
            $esp->save();
        }
        return redirect()->back();
    }
    public function saveall(Request $request){
        $idEstab = $request->id;
        $idsesp = $request->get('esp');
        if(!empty($idsesp))
        {
            foreach ($idsesp as $idEsp) {
                $esptoestab = new Esptoestab();
                $esptoestab->idEsp = $idEsp;
                $esptoestab->idEstab = $idEstab;
                $esptoestab->save();
            }
        }
        Session::flash('novo-estab', 'Estabelecimento cadastrado com sucesso!');
        return redirect()->route('estabelecimento');

    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Especialidade  $especialidade
     * @return \Illuminate\Http\Response
     */
    public function show(Especialidade $especialidade)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Especialidade  $especialidade
     * @return \Illuminate\Http\Response
     */
    public function editName(Request $request)
    {

        $esps = Especialidade::query();
        $esp = $esps->where('id', $request->idEdit)->first();
        $esp->nome = $request->nome;
        $esp->update();

        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Especialidade  $especialidade
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Especialidade $especialidade)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Especialidade  $especialidade
     * @return \Illuminate\Http\Response
     */
    public function destroy(Especialidade $especialidade)
    {
        //
    }
}
